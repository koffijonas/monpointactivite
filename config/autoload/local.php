<?php
/** ddd
 * Local Configuration Override
 *
 * This configuration override file is for overriding environment-specific and
 * security-sensitive configuration information. Copy this file without the
 * .dist extension at the end and populate values as needed.
 *
 * @NOTE: This file is ignored from Git by default with the .gitignore included
 * in ZendSkeletonApplication. This is a good practice, as it prevents sensitive
 * credentials from accidentally being committed into version control.
 */

use Doctrine\DBAL\Driver\PDOMySql\Driver as PDOMySqlDriver;

return [
    // Configuration for your SMTP server (for sending outgoing mail).
    'smtp' => [
        'name'              => 'localhost.localdomain',
        'host'              => '127.0.0.1',
        'port'              => 1025,
        'connection_class'  => 'plain',
        'connection_config' => [
            'username' => '<user>',
            'password' => '<pass>',
        ],
    ],
    // Database connection configuration.
    /*'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => PDOMySqlDriver::class,
                'params' => [
                    'host'     => 'localhost',
                    'user'     => 'root',
                    'password' => '',
                    'dbname'   => 'sygepadi',
                    'driverOptions' => array(
                        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                    )
                ]
            ],            
        ],        
    ],*/
    
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => PDOMySqlDriver::class,
                'params' => [
                    'host'     => '172.16.0.13',
                    'user'     => 'tiemoko1.doumbia',
                    'password' => 'safp204di',
                    'dbname'   => 'monPointActivite',
                    'driverOptions' => array(
                        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                    )
                ]
            ],            
        ],        
    ],
];
