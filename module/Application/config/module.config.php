<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'application' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/application[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller'    => Controller\IndexController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
            'pointactivites' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/pointactivites[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller'    => Controller\PointController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
            
            'addpointactivite' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/pointactivites/add',
                    'defaults' => [
                        'controller' => Controller\PointController::class,
                        'action'     => 'add',
                    ],
                ],
            ],
            
            'projects' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/projects[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller'    => Controller\ProjectController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
            
            'subDirections' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/subDirections[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller'    => Controller\SubDirectionController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
            
            'ajax' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/ajax[/:action[/:id][/:date]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller'    => Controller\AjaxController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
            
            'services' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/services[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller'    => Controller\ServiceController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
            
            'states' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/states[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller'    => Controller\StateController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
            
            'about' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/about',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'about',
                    ],
                ],
            ],            
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\AjaxController::class => Controller\Factory\AjaxControllerFactory::class,
            Controller\IndexController::class => Controller\Factory\IndexControllerFactory::class,
            Controller\PointController::class => Controller\Factory\PointControllerFactory::class,
            Controller\ProjectController::class => Controller\Factory\ProjectControllerFactory::class,
            Controller\SubDirectionController::class => Controller\Factory\SubDirectionControllerFactory::class,
            Controller\ServiceController::class => Controller\Factory\ServiceControllerFactory::class,
            Controller\StateController::class => Controller\Factory\StateControllerFactory::class,
        ],
    ],
    // The 'access_filter' key is used by the User module to restrict or permit
    // access to certain controller actions for unauthorized visitors.
    'access_filter' => [
        'options' => [
            // The access filter can work in 'restrictive' (recommended) or 'permissive'
            // mode. In restrictive mode all controller actions must be explicitly listed 
            // under the 'access_filter' config key, and access is denied to any not listed 
            // action for not logged in users. In permissive mode, if an action is not listed 
            // under the 'access_filter' key, access to it is permitted to anyone (even for 
            // not logged in users. Restrictive mode is more secure and recommended to use.
            'mode' => 'restrictive'
        ],
        'controllers' => [
            Controller\IndexController::class => [
                // Allow anyone to visit "index" and "about" actions
                ['actions' => ['index', 'about'], 'allow' => '*'],
                // Allow authorized users to visit "settings" action
                ['actions' => ['settings'], 'allow' => '@']
            ],
            
            Controller\AjaxController::class => [
                ['actions' => ['index','mesActivites','collaborateursSurPoint', 'getServices', 'exportExcel'], 
                    'allow' => '@'],
                ['actions' => ['exportSuiviExcel'], 'allow' => '+service.own.manage'],
                ['actions' => ['exportSousDirectionExcel'], 'allow' => '+subDirectionPoints.own.manage']
            ],
            
            Controller\PointController::class => [
                ['actions' => ['index', 'add', 'edit', 'delete'], 'allow' => '@'],
                ['actions' => ['pointsDeMesAgents'], 'allow' => '+service.own.manage'],
                ['actions' => ['pointsDeMesServices'], 'allow' => '+subDirectionPoints.own.manage']
            ],
            
            Controller\ProjectController::class => [
                ['actions' => ['index', 'add', 'edit', 'delete'], 'allow' => '+serviceProjects.manage']
            ],
            
            Controller\SubDirectionController::class => [
                ['actions' => ['index', 'add', 'edit', 'delete'], 'allow' => '+subDirection.manage']
            ],
            
            Controller\ServiceController::class => [
                ['actions' => ['index', 'add', 'edit', 'delete'], 'allow' => '+service.manage']
            ],
            
            Controller\StateController::class => [
                ['actions' => ['index', 'add', 'edit', 'delete'], 'allow' => '+service.manage']
            ],
            
        ]
    ],
    // This key stores configuration for RBAC manager.
    'rbac_manager' => [
        'assertions' => [Service\RbacAssertionManager::class],
    ],
    'service_manager' => [
        'factories' => [
            Service\AjaxManager::class => Service\Factory\AjaxManagerFactory::class,
            Service\NavManager::class => Service\Factory\NavManagerFactory::class,
            Service\RbacAssertionManager::class => Service\Factory\RbacAssertionManagerFactory::class,
            Service\PointManager::class => Service\Factory\PointManagerFactory::class,
            Service\ProjectManager::class => Service\Factory\ProjectManagerFactory::class,
            Service\SubDirectionManager::class => Service\Factory\SubDirectionManagerFactory::class,
            Service\ServiceManager::class => Service\Factory\ServiceManagerFactory::class,
            Service\StateManager::class => Service\Factory\StateManagerFactory::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            View\Helper\Menu::class => View\Helper\Factory\MenuFactory::class,
            View\Helper\Breadcrumbs::class => InvokableFactory::class,
        ],
        'aliases' => [
            'mainMenu' => View\Helper\Menu::class,
            'pageBreadcrumbs' => View\Helper\Breadcrumbs::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    // The following key allows to define custom styling for FlashMessenger view helper.
    'view_helper_config' => [
        'flashmessenger' => [
            'message_open_format'      => '<div%s><ul><li>',
            'message_close_string'     => '</li></ul></div>',
            'message_separator_string' => '</li><li>'
        ]
    ], 
    
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],
    
    'translator' => [
        'locale' => 'fr_FR',
        'translation_files' => [
            [
                'type'     => 'phparray',
                'filename' => __DIR__ . '/../../../data/language/fr/Zend_Validate.php',
            ],
        ],
    ],
];
