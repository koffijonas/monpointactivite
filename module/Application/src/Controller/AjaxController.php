<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\Jquery\DataTable\DataTable;
use Application\Entity\Service;
use Application\Entity\Validation;
use Application\Entity\SubDirection;
use User\Entity\User;
use PHPExcel_Style_Alignment;
use PHPExcel_Worksheet_Drawing;
use PHPExcel_Style_Border;

/**
 * This is the main controller class of the User Demo application. It contains
 * site-wide actions such as Home or About.
 */
class AjaxController extends AbstractActionController 
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    
    private $phpExcel;
    
    /**
     * Ajax manager.
     * @var Application\Service\AjaxManager 
     */
    private $controllerManager;
    
    /**
     * Point manager.
     * @var Application\Service\PointManager 
     */
    private $pointManager;
    
    /**
     * Constructor. Its purpose is to inject dependencies into the controller.
     */
    public function __construct($entityManager, $controllerManager, $pointManager, $phpExcel) {
       $this->entityManager = $entityManager;
       $this->controllerManager = $controllerManager;
       $this->phpExcel = $phpExcel;
       $this->pointManager = $pointManager;
    }
    
    public function indexAction() {
        
    }
    
    public function mesActivitesAction() {
        $response = $this->getResponse();
        ob_start();
        
        try {
            // Traitement
            $params['dateDebut'] = $this->getRequest()->getPost('start');
            $params['dateFin'] = $this->getRequest()->getPost('end');
            $params['executionState'] = $this->getRequest()->getPost('executionState');
            
            $dataTable = new DataTable($this->entityManager, $this->params()->fromQuery());
            $result = $dataTable->getDatatable(
                $this->controllerManager->getMesActivites($params)
            );
            
            $data = json_decode($result);
            foreach($data->aaData as $key => $ligne){
                $data->aaData[$key][] = count($this->entityManager->getRepository(\Application\Entity\Collaboration::class)->findBy(['pointId'=>$ligne[6]]))-1;
            }
            
            // get Validation status
        
            $query = $this->entityManager->createQueryBuilder();

            $query->select(['v.pointId', 'v.validationState'])
                    ->from(Validation::class, 'v')
                    ;

            if(empty($params['dateDebut']) && empty($params['dateFin']) && empty($params['executionState'])){
                $dernierLundi = date('Y-m-d', strtotime('monday'))==date('Y-m-d') ? date('Y-m-d', strtotime('monday')) : date('Y-m-d',strtotime('last monday'));

                $query->where('v.validationDate >= :dd')->setParameter('dd', $dernierLundi);

            }else if(!empty($params['dateDebut']) && !empty($params['dateFin'])){
                //$query->andWhere( $query->expr()->between('a.startDate', $params['dateDebut'], $params['dateFin']) );
                $query->andWhere('(v.validationDate >= ?1')->setParameter(1, $params['dateDebut']);
                $query->andWhere('v.validationDate <= ?2)')->setParameter(2, $params['dateFin']);


            }else{
                if(isset($params['dateDebut']) && !empty($params['dateDebut'])){
                    $query->andWhere('v.validationDate >= ?1')->setParameter(1, $params['dateDebut']);
                }

                if(isset($params['dateFin']) && !empty($params['dateFin'])){
                    $query->andWhere('v.validationDate <= ?3')->setParameter(3, $params['dateFin']);
                }
            }

            $results = $query->getQuery()->getResult();
            $validatedPointIds = array();
            foreach ($results as $res){
                $validatedPointIds[] = $res['pointId'];
            }
            
            foreach($data->aaData as $key => $ligne){
                if(in_array($ligne[6], $validatedPointIds)) {
                     $data->aaData[$key][] = 1;  
                }else{
                    $data->aaData[$key][] = 0;
                }
            }
            
            //$t = $this->entityManager->getRepository(\Application\Entity\Collaboration::class)->findBy(['pointId'=>$data->aaData[0][6]]);
            //var_dump( json_encode($data) ); die;
            
            // Affichage
            $response->setStatusCode(200);
            header('Content-Type: application/json; charset=utf-8');
            //echo \Zend\Json\Encoder::encode($data);
            //echo $result;
            echo json_encode($data);
            
        } catch (Exception $e) {
            $response->setStatusCode(500);
            echo $e->getTraceAsString();
        }
        
        $response->setContent(ob_get_clean());
        return $response;
    }
    
    public function collaborateursSurPointAction() {
        $response = $this->getResponse();
        ob_start();
        
        try {
            // Traitement
            $idPoint = $this->getRequest()->getPost('id');
            
            $query = $this->entityManager->createQueryBuilder();
            $query->select(['u.id as userId','u.fullName','c.id as collabId'])
                    ->from(\Application\Entity\User::class, 'u')
                    ->leftJoin(\Application\Entity\Collaboration::class, 'c', 'WITH', 'c.userId=u.id AND c.pointId= :pid')
                    ->where('u.service = :usrv')
                    ->andWhere('u.id != :uid')
                    ->setParameter('usrv', $this->currentUser()->getService())
                    ->setParameter('pid', $idPoint)
                    ->setParameter('uid', $this->currentUser()->getId())
                    ->orderBy('c.id', 'desc')
                    ;
            
            $rs = $query->getQuery();
            $result['listeCollab'] = $rs->getResult(); 
            
            $point = $this->entityManager->getRepository(\Application\Entity\Point::class)->find($idPoint);
            $result['propietaireId'] = $point->getUserId();
            //var_dump($result); die;
            
            // Affichage
            $response->setStatusCode(200);
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($result);
            
        } catch (Exception $e) {
            $response->setStatusCode(500);
            echo $e->getTraceAsString();
        }
        
        $response->setContent(ob_get_clean());
        return $response;
    }
    
    public function getServicesAction() {
        $response = $this->getResponse();
        ob_start();
        
        try {
            // Traitement
            $userFunction = $this->getRequest()->getPost('userFunction');
            if($userFunction == "Sous-Directeur"){
                $allServices = $this->entityManager->getRepository(SubDirection::class)
                        ->findBy([], ['libSousDirection'=>'ASC']);
                $serviceList = [];
                foreach ($allServices as $service) {
                    $serviceList[$service->getId()] = $service->getLibSousDirection();
                }
            }
            else{
                
                $allServices = $this->entityManager->getRepository(Service::class)
                        ->findBy([], ['serviceName'=>'ASC']);
                $serviceList = [];
                foreach ($allServices as $service) {
                    $serviceList[$service->getId()] = $service->getServiceName();
                }
            }
            // Affichage
            $response->setStatusCode(200);
            header('Content-Type: application/json; charset=utf-8');
            //echo \Zend\Json\Encoder::encode($data);
            echo json_encode($serviceList);
            
            
        } catch (Exception $e) {
            $response->setStatusCode(500);
            echo $e->getTraceAsString();
        }
        
        $response->setContent(ob_get_clean());
        return $response;
    }
    
    public function exportExcelAction() {
        $response = $this->getResponse();
       try {
            // Traitement
            /*$start = $this->params()->fromRoute('id', -1);
            $end = $this->params()->fromRoute('date', -1);
            
            $params = [];
            if ($start != -1){
                $start = substr_replace($start, '-', 4, 0);
                $start = substr_replace($start, '-', 7, 0);
                
                $params['dateDebut'] = $start;
            }
            
            if ($end != -1){
                
                $end = substr_replace($end, '-', 4, 0);
                $end = substr_replace($end, '-', 7, 0);
                
                $params['dateFin'] = $end;
            }*/
           
            if($this->getRequest()->isPost()){
                $params['dateDebut'] = $this->getRequest()->getPost('start');
                $params['dateFin'] = $this->getRequest()->getPost('end');
            }
            
            $params['isExportExcel'] = true;
            
            $query = $this->controllerManager->getMesActivites($params);
            
            $rs = $query->getQuery();
            
            
            $result = $rs->getResult();
            if(count($result) == 0){
                echo 'Vous n\'avez aucun point pour cette prériode.';
            }
            else{
                $center = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                    'borders' => array(
                        'allborders' => array(
                          'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 11,
                    )
                );
                
                $boldTitle = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                    'font' => array(
                        'bold' => true,
                        'name' => 'Times New Roman',
                        'size' => 14,
                    )
                );
                
                $boldHeader = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                    'borders' => array(
                        'allborders' => array(
                          'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 11,
                        'bold' => true,
                    )
                );
                
                $bold = array(
                    'font' => array(
                        'bold' => true,
                    )
                );
                
                $user = $this->currentUser();
                $objPHPExcel = $this->excelHeaderCreate($user);
                
                if(empty($params['dateDebut']) && empty($params['dateFin'])){
                    $dernierLundi = date('Y-m-d', strtotime('monday'))==date('Y-m-d') ? date('d/m/Y', strtotime('monday')) : date('d/m/Y',strtotime('last monday'));
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E16', 'POINTS DES ACTIVITES DE LA SEMAINE DU '.$dernierLundi );
                    $fileName = 'Point des activités de la semaine du '.$dernierLundi;
                }else{
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E16', 'POINTS DES ACTIVITES DU '.$params['dateDebut'].' au '.$params['dateFin'] );
                    $fileName = 'Point des activités du '.$params['dateDebut'].' au '.$params['dateFin'];
                }
                
                $objPHPExcel->getActiveSheet()->mergeCells('A18:C19');
                $objPHPExcel->getActiveSheet()->mergeCells('D18:G19');
                $objPHPExcel->getActiveSheet()->mergeCells('H18:K18');
                $objPHPExcel->getActiveSheet()->mergeCells('H19:I19');
                $objPHPExcel->getActiveSheet()->mergeCells('J19:K19');
                $objPHPExcel->getActiveSheet()->mergeCells('L18:M19');
                $objPHPExcel->getActiveSheet()->mergeCells('N18:P19');
                
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A18', 'DOMAINE / REUNION / PROJET')
                    ->setCellValue('D18', 'ACTIVITES')
                    ->setCellValue('H18', 'ECHEANCE')
                    ->setCellValue('H19', 'Date de début')
                    ->setCellValue('J19', 'Date de fin')
                    ->setCellValue('L18', 'ETAT D\'EXECUTION')
                    ->setCellValue('N18', 'OBSERVATION');
                
                $objPHPExcel->getActiveSheet()->getStyle('A18')->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('L18')->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('E16')->applyFromArray($boldTitle);
                $objPHPExcel->getActiveSheet()->getStyle('A18:A19')->applyFromArray($boldHeader);
                $objPHPExcel->getActiveSheet()->getStyle('A18:P19')->applyFromArray($boldHeader);
                $objPHPExcel->getActiveSheet()->getStyle('I18')->applyFromArray($boldHeader);
                $objPHPExcel->getActiveSheet()->getStyle('H19:J19')->applyFromArray($boldHeader);
                //remplissage du tableau avec les éléments de la base de données
                $rowIndex = 20;
                $next = 21;
                $projetGroupeLabel = $result[0]['projectName'];
                $firstProjetGroupeIndex = $rowIndex;
                //var_dump($projetGroupeLabel); die;
                foreach ($result as $res){
                    
                    $res['expiryDate'] = implode('/', array_reverse(explode('-', $res['expiryDate'])));
                    $res['startDate'] = implode('/', array_reverse(explode('-', $res['startDate'])));
                    $res['endDate'] = implode('/', array_reverse(explode('-', $res['endDate'])));
                        
                    //Fusionne le libellé du projet s'il y a plusieurs ligne du même projet
                    if($projetGroupeLabel != $res['projectName']){
                        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$firstProjetGroupeIndex.':C'.($rowIndex-1));
                        $projetGroupeLabel = $res['projectName'];
                        $firstProjetGroupeIndex = $rowIndex;
                    }
                    
                    //$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$rowIndex.':C'.$rowIndex);
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$rowIndex.':G'.$rowIndex);
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$rowIndex.':I'.$rowIndex);
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J'.$rowIndex.':K'.$rowIndex);
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('L'.$rowIndex.':M'.$rowIndex);
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('N'.$rowIndex.':P'.$rowIndex);
                                        
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$rowIndex, $res['projectName']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$rowIndex, $res['activity']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$rowIndex, $res['startDate']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$rowIndex, $res['endDate']=='00/00/0000'?'-':$res['endDate'] );
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$rowIndex, $res['stateName']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$rowIndex, $res['observation']);
                    
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIndex)->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle('D'.$rowIndex)->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle('N'.$rowIndex)->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIndex)->applyFromArray($bold);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIndex.':A'.$rowIndex)->applyFromArray($center);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$rowIndex.':P'.$next)->applyFromArray($center);
                    $objPHPExcel->getActiveSheet()->getStyle('I'.$rowIndex)->applyFromArray($center);
                    $objPHPExcel->getActiveSheet()->getStyle('H'.$next.':J'.$next)->applyFromArray($center);
                    
                    $rowIndex++; $next++;
                }
                
                //Last ligne merge
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$firstProjetGroupeIndex.':C'.($rowIndex-1));
                
                
                $objWriter = $this->phpExcel->createWriter($objPHPExcel, 'Excel2007' );
                $response = $this->phpExcel->createHttpResponse($objWriter, 200, [
                    'Pragma' => 'public',
                    'Cache-control' => 'must-revalidate, post-check=0, pre-check=0',
                    'Cache-control' => 'private',
                    'Expires' => '0000-00-00',
                    'Content-Type' => 'application/vnd.ms-excel; charset=utf-8',
                    'Content-Disposition' => 'attachment; filename=' . $fileName.'.xlsx',
                    ]);
            }
            
        } catch (Exception $e) {
            $response->setStatusCode(500);
            echo $e->getTraceAsString();
        }
        return $response;
    }

    public function excelHeaderCreate($user){
        $center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $objPHPExcel = $this->phpExcel->createPHPExcelObject();
        $objPHPExcel->getProperties()->setCreator("MONPOINT")
            ->setLastModifiedBy("MONPOINT")
            ->setTitle("Gestion des points d'activités")
            ->setSubject("Gestion des points d'activités")
            ->setDescription("Document généré par l'application MONPOINT")
            ->setKeywords("Point d'activités")
            ->setCategory("Point d'activités");

        $objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
        $objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
        $objPHPExcel->getActiveSheet()->mergeCells('A3:E3');
        $objPHPExcel->getActiveSheet()->mergeCells('A4:E4');
        $objPHPExcel->getActiveSheet()->mergeCells('A5:E5');
        $objPHPExcel->getActiveSheet()->mergeCells('A6:E6');
        $objPHPExcel->getActiveSheet()->mergeCells('A7:E7');
        $objPHPExcel->getActiveSheet()->mergeCells('A8:E8');
        $objPHPExcel->getActiveSheet()->mergeCells('A9:E9');
        $objPHPExcel->getActiveSheet()->mergeCells('A10:E10');
        $objPHPExcel->getActiveSheet()->mergeCells('A11:E11');
        $objPHPExcel->getActiveSheet()->mergeCells('A12:E12');
        $objPHPExcel->getActiveSheet()->mergeCells('A13:E13');
        $objPHPExcel->getActiveSheet()->mergeCells('A14:E14');
        $objPHPExcel->getActiveSheet()->mergeCells('N1:P1');
        $objPHPExcel->getActiveSheet()->mergeCells('N2:P2');
        $objPHPExcel->getActiveSheet()->mergeCells('N3:P3');
        $objPHPExcel->getActiveSheet()->mergeCells('N9:P9');
        $objPHPExcel->getActiveSheet()->mergeCells('E16:M16');

        $userService = $this->entityManager->getRepository(Service::class)
                ->find($user->getService());

        $userSubDirection = $this->entityManager->getRepository(SubDirection::class)
                ->find($userService->getSubDirectionId());
                
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'SECRETARIAT D\'ETAT AUPRES DU PREMIER MINISTRE,')
            ->setCellValue('A2', 'CHARGE DU BUDGET')
            ->setCellValue('A3', 'ET DU PORTEFEUILLE DE L\'ETAT')
            ->setCellValue('A4', '------');

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A5', 'DIRECTION GENERALE DES IMPOTS')
            ->setCellValue('A6', '------')
            ->setCellValue('A7', 'DIRECTION DE L\'INFORMATIQUE')
            ->setCellValue('A8', '------')
            ->setCellValue('A9', strtoupper($userSubDirection->getLibSousDirection()))
            ->setCellValue('A10', '------')
            ->setCellValue('A11', strtoupper($userService->getServiceName()))
            ->setCellValue('A12', '------')
            ->setCellValue('N1', 'REPUBLIQUE DE COTE D\'IVOIRE')
            ->setCellValue('N2', 'Union - Discipline - Travail')
            ->setCellValue('N3', '------')
            ->setCellValue('N9', 'Abidjan, le '. date('d/m/Y'));
        
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('PHPExcel image');
        $objDrawing->setDescription('PHPExcel image');
        $logoDGIpath = './public/assets/img/logoDGI.png';
        $objDrawing->setPath($logoDGIpath);
        $objDrawing->setHeight(50);
        $objDrawing->setCoordinates($objPHPExcel->getActiveSheet()->getCell('H1')->getCoordinate());
        $objDrawing->setOffsetX(95);
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
        $objPHPExcel->getActiveSheet()->getStyle('A1:A14')->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->getStyle('N1:N3')->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->getStyle('N9')->applyFromArray($center);
        $objPHPExcel->getActiveSheet()->getStyle('M16')->applyFromArray($center);
        //$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
        $objPHPExcel->getActiveSheet()->getStyle('A9')->getAlignment()->setWrapText(true);
        //$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
        $objPHPExcel->getActiveSheet()->getStyle('A11')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setTitle('Mon point de la semaine');
        $objPHPExcel->setActiveSheetIndex(0);
        
        return $objPHPExcel;
    }
    
    public function exportSuiviExcelAction() {
        $response = $this->getResponse();
       try {
           $user = $this->currentUser();
           $params = [
                'user'=>$user
            ];
            $myServiceUsers = $this->entityManager->getRepository(User::class)
                    ->findBy(['service'=>$user->getService()], ['fullName'=>'ASC']);
            if($this->getRequest()->isPost()){
                $params['dateDebut'] = $this->getRequest()->getPost('start');
                $params['dateFin'] = $this->getRequest()->getPost('end');
                $params['agent'] = $this->getRequest()->getPost('agent');
            }
            $params['isExportExcel'] = true;
            $allPoints = $this->pointManager->getActivitesDesAgents($params);
            
            if(count($allPoints) == 0){
                echo 'Vous n\'avez aucun point pour cette prériode.';
            }
            else{
                $style = array(
                    'borders' => array(
                        'allborders' => array(
                          'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 11,
                    )
                );
                
                $center = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                    'borders' => array(
                        'allborders' => array(
                          'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 11,
                    )
                );
                
                $boldTitle = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                    'font' => array(
                        'bold' => true,
                        'name' => 'Times New Roman',
                        'size' => 14,
                    )
                );
                
                $boldHeader = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                    'borders' => array(
                        'allborders' => array(
                          'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 11,
                        'bold' => true,
                    )
                );
                
                $title = array(
                    'font' => array(
                        'name' => 'Century Gothic',
                        'size' => 36,
                        'bold' => true,
                    )
                );
                
                $bold = array(
                    'font' => array(
                        'bold' => true,
                    )
                );
                
                $userService = $this->entityManager->getRepository(Service::class)
                        ->find($user->getService());
                
                $userSubDirection = $this->entityManager->getRepository(SubDirection::class)
                        ->find($userService->getSubDirectionId());

                $objPHPExcel = $this->phpExcel->createPHPExcelObject();
                
                $objPHPExcel->getProperties()->setCreator("MONPOINT")
                    ->setLastModifiedBy("MONPOINT")
                    ->setTitle("Gestion des points d'activités")
                    ->setSubject("Gestion des points d'activités")
                    ->setDescription("Document généré par l'application MONPOINT")
                    ->setKeywords("Point d'activités")
                    ->setCategory("Point d'activités");

                $mois=['Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'];
                
                $debutJour = date('d', strtotime('last monday'));
                $debutMois = date('n', strtotime('last monday'));
                $debutMois = $mois[$debutMois-1];
                $finJour = date('d');
                $finMois = date('n');
                $finMois = $mois[$finMois-1];
                $finYear = date('Y');

                //liste des tâches
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(21.14);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(17);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(21.71);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(21.77);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(23);

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'SOUS-DIRECTION :')
                    ->setCellValue('B1', strtoupper(ltrim($userSubDirection->getLibSousDirection(), 'Sous-Direction')))
                    ->setCellValue('A2', 'SERVICE : ')
                    ->setCellValue('B2', strtoupper(ltrim($userService->getServiceName(), 'Service des')))
                    ->setCellValue('A3', 'SEMAINE : ')
                    ->setCellValue('B3', "Du ".$debutJour." ".$debutMois." au ".$finJour." ".$finMois." ".$finYear);

                //$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
                $objPHPExcel->getActiveSheet()->getStyle('A1:A6')->applyFromArray($bold);
                
                $objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->setWrapText(true);
                //$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
                $objPHPExcel->getActiveSheet()->setTitle('Liste des tâches');
                $objPHPExcel->setActiveSheetIndex(0);
                
                //var_dump($result);die;
                //remplissage du tableau avec les éléments de la base de données
                $bodyIndex = 4;
                foreach ($myServiceUsers as $myServiceUser){
                    $startIndex = $bodyIndex + 2;
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$startIndex, 'AGENT :')
                        ->setCellValue('B'.$startIndex, strtoupper($myServiceUser->getFullname()))
                        ->setCellValue('A'.($startIndex+1), 'POSTE :');
                    
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$startIndex.':A'.($startIndex+1))->applyFromArray($bold);
                      
                    $headerIndex = $startIndex+3;
                    $bodyIndex = $startIndex+4;
                    
                    //table header
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$headerIndex, 'Tâche')
                            ->setCellValue('B'.$headerIndex, 'Priorité')
                            ->setCellValue('C'.$headerIndex, 'Etat')
                            ->setCellValue('D'.$headerIndex, 'Date de début')
                            ->setCellValue('E'.$headerIndex, 'Date d\'échéance')
                            ->setCellValue('F'.$headerIndex, 'Pourcentage achevé')
                            ->setCellValue('G'.$headerIndex, 'Terminée/En Retard')
                            ->setCellValue('H'.$headerIndex, 'Notes');
                    
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$headerIndex.':H'.$headerIndex)->applyFromArray($boldHeader);
                        
                    foreach ($allPoints as $point)
                    {
                        
                        //table body
                        if($point['email'] == $myServiceUser->getEmail()){
                            
                            $point['expiryDate'] = implode('/', array_reverse(explode('-', $point['expiryDate'])));
                            $point['startDate'] = implode('/', array_reverse(explode('-', $point['startDate'])));
                            $point['endDate'] = implode('/', array_reverse(explode('-', $point['endDate'])));
                            
                            if($point['stateName'] == 'Terminé'){
                                $state = 'Terminé';
                            }elseif($point['expiryDate']<$point['endDate']){
                                $state = 'En Retard';
                            }else{
                                $state = '';
                            }
                            
                            $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$bodyIndex, strtoupper($point['projectName'])." : ".$point['activity'])
                                ->setCellValue('B'.$bodyIndex, 'Elévé')
                                ->setCellValue('C'.$bodyIndex, $point['stateName'])
                                ->setCellValue('D'.$bodyIndex, $point['startDate'])
                                ->setCellValue('E'.$bodyIndex, $point['expiryDate']=='00/00/0000'?'-':$point['expiryDate'])
                                ->setCellValue('F'.$bodyIndex, $point['purcent'].'%')
                                ->setCellValue('G'.$bodyIndex, $state)
                                ->setCellValue('H'.$bodyIndex, $point['observation']);

                            $objPHPExcel->getActiveSheet()->getStyle('A'.$bodyIndex)->getAlignment()->setWrapText(true);
                            $objPHPExcel->getActiveSheet()->getStyle('H'.$bodyIndex)->getAlignment()->setWrapText(true);
                            $objPHPExcel->getActiveSheet()->getStyle('B'.$bodyIndex.':H'.$bodyIndex)->applyFromArray($center);
                            $objPHPExcel->getActiveSheet()->getStyle('A'.$bodyIndex)->applyFromArray($style);
                        
                            $bodyIndex++;
                        }

                    }
                    
                }
                
                // recapitulatif du service
                $objPHPExcel->createSheet(1);
                $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A1', 'DIRECTION DE L\'INFORMATIQUE')
                    ->setCellValue('A2', 'SOUS-DIRECTION :')
                    ->setCellValue('B2', strtoupper(ltrim($userSubDirection->getLibSousDirection(), 'Sous-Direction')))
                    ->setCellValue('A3', 'SERVICE : ')
                    ->setCellValue('B3', strtoupper(ltrim($userService->getServiceName(), 'Service des')))
                    ->setCellValue('A4', 'SEMAINE : ')
                    ->setCellValue('B4', "Du ".$debutJour." ".$debutMois." au ".$finJour." ".$finMois." ".$finYear)
                    ->setCellValue('A6', 'LISTE DES TÂCHES')
                    ->setCellValue('H6', '2019');

                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(21.14);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(17);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(21.71);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(21.77);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(23);

                //$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
                $objPHPExcel->getActiveSheet()->getStyle('A1:A4')->applyFromArray($bold);
                $objPHPExcel->getActiveSheet()->getStyle('A6:H6')->applyFromArray($title);
                $objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->setWrapText(true);
                //$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
                $objPHPExcel->getActiveSheet()->setTitle('Recapitulatif du service');
                $objPHPExcel->setActiveSheetIndex(1);
                
                $headerIndex = 8;
                $bodyIndex = 9;

                //table header
                $objPHPExcel->setActiveSheetIndex(1)
                        ->setCellValue('A'.$headerIndex, 'Tâche')
                        ->setCellValue('B'.$headerIndex, 'Priorité')
                        ->setCellValue('C'.$headerIndex, 'Etat')
                        ->setCellValue('D'.$headerIndex, 'Date de début')
                        ->setCellValue('E'.$headerIndex, 'Date d\'échéance')
                        ->setCellValue('F'.$headerIndex, 'Pourcentage achevé')
                        ->setCellValue('G'.$headerIndex, 'Terminée/En Retard')
                        ->setCellValue('H'.$headerIndex, 'Notes');

                $objPHPExcel->getActiveSheet()->getStyle('A'.$headerIndex.':H'.$headerIndex)->applyFromArray($boldHeader);
                
                //supprimer les points dupliqués dû à la collaboration
                $allPointsWithoutDuplicate = array(); // le nouveau tableau dédoublonné
                $pointIds = array(); // contiendra les ids à éviter
                foreach($allPoints as $point)
                {
                  if(!in_array($point['id'], $pointIds)) {
                     $allPointsWithoutDuplicate[] = $point;
                     $pointIds[] = $point['id'];   
                  }
                }
                
                foreach ($allPointsWithoutDuplicate as $point)
                {
                    $point['expiryDate'] = implode('/', array_reverse(explode('-', $point['expiryDate'])));
                    $point['startDate'] = implode('/', array_reverse(explode('-', $point['startDate'])));
                    $point['endDate'] = implode('/', array_reverse(explode('-', $point['endDate'])));
                    
                    if($point['stateName'] == 'Terminé'){
                        $state = 'Terminé';
                    }elseif($point['expiryDate']<$point['endDate']){
                        $state = 'En Retard';
                    }else{
                        $state = '';
                    }
                            
                    //table body
                    $objPHPExcel->setActiveSheetIndex(1)
                        ->setCellValue('A'.$bodyIndex, strtoupper($point['projectName'])." : ".$point['activity'])
                        ->setCellValue('B'.$bodyIndex, 'Elévé')
                        ->setCellValue('C'.$bodyIndex, $point['stateName'])
                        ->setCellValue('D'.$bodyIndex, $point['startDate'])
                        ->setCellValue('E'.$bodyIndex, $point['expiryDate']=='00/00/0000'?'-':$point['expiryDate'])
                        ->setCellValue('F'.$bodyIndex, $point['purcent'].'%')
                        ->setCellValue('G'.$bodyIndex, $state)
                        ->setCellValue('H'.$bodyIndex, $point['observation']);

                    $objPHPExcel->getActiveSheet()->getStyle('A'.$bodyIndex)->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle('H'.$bodyIndex)->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle('B'.$bodyIndex.':H'.$bodyIndex)->applyFromArray($center);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$bodyIndex)->applyFromArray($style);

                    $bodyIndex++;
                    
                }
                
                if(empty($params['dateDebut']) && empty($params['dateFin'])){
                    $dernierLundi = date('Y-m-d', strtotime('monday'))==date('Y-m-d') ? date('d/m/Y', strtotime('monday')) : date('d/m/Y',strtotime('last monday'));
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E16', 'POINTS DES ACTIVITES DE LA SEMAINE DU '.$dernierLundi );
                    $fileName = 'Point des activités des agents de la semaine du '.$dernierLundi;
                }else{
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E16', 'POINTS DES ACTIVITES DU '.$params['dateDebut'].' au '.$params['dateFin'] );
                    $fileName = 'Point des activités des agents du '.$params['dateDebut'].' au '.$params['dateFin'];
                }
                
                $objWriter = $this->phpExcel->createWriter($objPHPExcel, 'Excel2007' );

                $response = $this->phpExcel->createHttpResponse($objWriter, 200, [
                    'Pragma' => 'public',
                    'Cache-control' => 'must-revalidate, post-check=0, pre-check=0',
                    'Cache-control' => 'private',
                    'Expires' => '0000-00-00',
                    'Content-Type' => 'application/vnd.ms-excel; charset=utf-8',
                    'Content-Disposition' => 'attachment; filename=' . $fileName.'.xlsx',
                ]);
            }
            
        } catch (Exception $e) {
            $response->setStatusCode(500);
            echo $e->getTraceAsString();
        }
        return $response;
    }
    
    public function exportSousDirectionExcelAction() {
        $response = $this->getResponse();
       try {
           $user = $this->currentUser();
           $params = [
                'user'=>$user
            ];
            $mySubDirectionServices = $this->entityManager->getRepository(Service::class)
                ->findBy(['subDirectionId'=>$user->getService()], ['serviceName'=>'ASC']);
            if($this->getRequest()->isPost()){
                $params['dateDebut'] = $this->getRequest()->getPost('start');
                $params['dateFin'] = $this->getRequest()->getPost('end');
                $params['service'] = $this->getRequest()->getPost('service');
            }
            $params['isExportExcel'] = true;
            $LesPoints = $this->pointManager->getActivitesDesServices($params);
            
            // get Validation status
        
            $query = $this->entityManager->createQueryBuilder();

            $query->select(['v.pointId', 'v.validationState'])
                    ->from(Validation::class, 'v')
                    ;

            if(empty($params['dateDebut']) && empty($params['dateFin'])){
                $dernierLundi = date('Y-m-d', strtotime('monday'))==date('Y-m-d') ? date('Y-m-d', strtotime('monday')) : date('Y-m-d',strtotime('last monday'));

                $query->where('v.validationDate >= :dd')->setParameter('dd', $dernierLundi);

            }else if(!empty($params['dateDebut']) && !empty($params['dateFin'])){
                //$query->andWhere( $query->expr()->between('a.startDate', $params['dateDebut'], $params['dateFin']) );
                $query->andWhere('(v.validationDate >= ?1')->setParameter(1, $params['dateDebut']);
                $query->andWhere('v.validationDate <= ?2)')->setParameter(2, $params['dateFin']);


            }else{
                if(isset($params['dateDebut']) && !empty($params['dateDebut'])){
                    $query->andWhere('v.validationDate >= ?1')->setParameter(1, $params['dateDebut']);
                }

                if(isset($params['dateFin']) && !empty($params['dateFin'])){
                    $query->andWhere('v.validationDate <= ?3')->setParameter(3, $params['dateFin']);
                }
            }

            $results = $query->getQuery()->getResult();
            $allValidationsForThisWeek = [];
            foreach ($results as $res){
                $allValidationsForThisWeek[$res['pointId']] = $res['validationState'];
            }
            
            //récupérer les points validées
            $allPoints = array();
            $points = array();
            $pointIds = array();
            foreach($LesPoints as $point)
            {
              if(array_key_exists($point['id'], $allValidationsForThisWeek)){
                 
                  $allPoints [] = $point;
              }

            }
            
            
            if(count($allPoints) == 0){
                echo 'Ces points n\'ont pas été validés.';
            }
            else{
                $style = array(
                    'borders' => array(
                        'allborders' => array(
                          'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 11,
                    )
                );
                
                $center = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                    'borders' => array(
                        'allborders' => array(
                          'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 11,
                    )
                );
                
                $boldTitle = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                    'font' => array(
                        'bold' => true,
                        'name' => 'Times New Roman',
                        'size' => 14,
                    )
                );
                
                $boldHeader = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                    'borders' => array(
                        'allborders' => array(
                          'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    ),
                    'font' => array(
                        'name' => 'Arial',
                        'size' => 11,
                        'bold' => true,
                    )
                );
                
                $title = array(
                    'font' => array(
                        'name' => 'Century Gothic',
                        'size' => 36,
                        'bold' => true,
                    )
                );
                
                $bold = array(
                    'font' => array(
                        'bold' => true,
                    )
                );
                
                $userSubDirection = $this->entityManager->getRepository(SubDirection::class)
                        ->find($user->getService());

                $objPHPExcel = $this->phpExcel->createPHPExcelObject();
                
                $objPHPExcel->getProperties()->setCreator("MONPOINT")
                    ->setLastModifiedBy("MONPOINT")
                    ->setTitle("Gestion des points d'activités")
                    ->setSubject("Gestion des points d'activités")
                    ->setDescription("Document généré par l'application MONPOINT")
                    ->setKeywords("Point d'activités")
                    ->setCategory("Point d'activités");

                $mois=['Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'];
                
                $debutJour = date('d', strtotime('last monday'));
                $debutMois = date('n', strtotime('last monday'));
                $debutMois = $mois[$debutMois-1];
                $finJour = date('d');
                $finMois = date('n');
                $finMois = $mois[$finMois-1];
                $finYear = date('Y');
                
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'DIRECTION DE L\'INFORMATIQUE')
                    ->setCellValue('A2', 'SOUS-DIRECTION :')
                    ->setCellValue('B2', strtoupper(ltrim($userSubDirection->getLibSousDirection(), 'Sous-Direction')))
                    ->setCellValue('A3', 'SEMAINE : ')
                    ->setCellValue('B3', "Du ".$debutJour." ".$debutMois." au ".$finJour." ".$finMois." ".$finYear)
                    ->setCellValue('A6', 'LISTE DES TÂCHES')
                    ->setCellValue('I6', '2019');

                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(21.14);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(17);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(21.71);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(21.77);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(23);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);

                //$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
                $objPHPExcel->getActiveSheet()->getStyle('A1:A4')->applyFromArray($bold);
                $objPHPExcel->getActiveSheet()->getStyle('A6:I6')->applyFromArray($title);
                $objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->setWrapText(true);
                //$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
                $objPHPExcel->getActiveSheet()->setTitle('Recapitulatif de la S-Direction');
                $objPHPExcel->setActiveSheetIndex(0);
                
                //supprimer les points dupliqués dû à la collaboration
                $pointAgents = array();
                $points = array();
                $pointIds = array();
                foreach($allPoints as $point)
                {
                  $pointAgents = [];
                  if(!in_array($point['id'], $pointIds)){
                    foreach($allPoints as $res)
                    {
                      if($point['id'] == $res['id']) {
                         $pointAgents[] = $res['fullName'];   
                      }
                    }
                    $point['agents'] = $pointAgents;
                    $points[] = $point;
                    $pointIds[] = $point['id'];
                  }
                  
                }
                
                $bodyIndex = 6;
                foreach ($mySubDirectionServices as $mySubDirectionService){
                    $startIndex = $bodyIndex + 2;
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$startIndex, 'Service :')
                        ->setCellValue('B'.$startIndex, strtoupper($mySubDirectionService->getServiceName()));
                    
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$startIndex.':A'.($startIndex+1))->applyFromArray($bold);
                      
                    $headerIndex = $startIndex+3;
                    $bodyIndex = $startIndex+4;
                    
                    //table header
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$headerIndex, 'Tâche')
                            ->setCellValue('B'.$headerIndex, 'Priorité')
                            ->setCellValue('C'.$headerIndex, 'Etat')
                            ->setCellValue('D'.$headerIndex, 'Date de début')
                            ->setCellValue('E'.$headerIndex, 'Date d\'échéance')
                            ->setCellValue('F'.$headerIndex, 'Pourcentage achevé')
                            ->setCellValue('G'.$headerIndex, 'Terminée/En Retard')
                            ->setCellValue('H'.$headerIndex, 'Notes')
                            ->setCellValue('H'.$headerIndex, 'Agents concernés');
                    
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$headerIndex.':I'.$headerIndex)->applyFromArray($boldHeader);
                        
                    foreach ($points as $point)
                    {
                        
                        $pointAgents = $point['agents'][0];
                        for($i=1; $i< count($point['agents']); $i++){
                            $pointAgents .= "\n\n".$point['agents'][$i];
                        }
                        //table body
                        if($point['service'] == $mySubDirectionService->getId()){
                            
                            $point['expiryDate'] = implode('/', array_reverse(explode('-', $point['expiryDate'])));
                            $point['startDate'] = implode('/', array_reverse(explode('-', $point['startDate'])));
                            $point['endDate'] = implode('/', array_reverse(explode('-', $point['endDate'])));
                        
                            $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$bodyIndex, strtoupper($point['projectName'])." : ".$point['activity'])
                                ->setCellValue('B'.$bodyIndex, 'Elévé')
                                ->setCellValue('C'.$bodyIndex, $point['stateName'])
                                ->setCellValue('D'.$bodyIndex, $point['startDate'])
                                ->setCellValue('E'.$bodyIndex, $point['expiryDate']=='00/00/0000'?'-':$point['expiryDate'])
                                ->setCellValue('F'.$bodyIndex, $point['purcent'].'%')
                                ->setCellValue('G'.$bodyIndex, '')
                                ->setCellValue('H'.$bodyIndex, $point['observation'])
                                ->setCellValue('I'.$bodyIndex, $pointAgents);
                            

                            $objPHPExcel->getActiveSheet()->getStyle('A'.$bodyIndex)->getAlignment()->setWrapText(true);
                            $objPHPExcel->getActiveSheet()->getStyle('H'.$bodyIndex)->getAlignment()->setWrapText(true);
                            $objPHPExcel->getActiveSheet()->getStyle('I'.$bodyIndex)->getAlignment()->setWrapText(true);
                            $objPHPExcel->getActiveSheet()->getStyle('B'.$bodyIndex.':I'.$bodyIndex)->applyFromArray($center);
                            $objPHPExcel->getActiveSheet()->getStyle('A'.$bodyIndex)->applyFromArray($style);
                        
                            $bodyIndex++;
                        }

                    }
                    
                }
                
                $objPHPExcel->createSheet(1);
                $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A1', 'DIRECTION DE L\'INFORMATIQUE')
                    ->setCellValue('A2', 'SOUS-DIRECTION :')
                    ->setCellValue('B2', strtoupper(ltrim($userSubDirection->getLibSousDirection(), 'Sous-Direction')))
                    ->setCellValue('A3', 'SEMAINE : ')
                    ->setCellValue('B3', "Du ".$debutJour." ".$debutMois." au ".$finJour." ".$finMois." ".$finYear)
                    ->setCellValue('A6', 'LISTE DES TÂCHES')
                    ->setCellValue('I6', '2019');

                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(21.14);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(17);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(21.71);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(21.77);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(23);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);

                //$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
                $objPHPExcel->getActiveSheet()->getStyle('A1:A4')->applyFromArray($bold);
                $objPHPExcel->getActiveSheet()->getStyle('A6:I6')->applyFromArray($title);
                $objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->setWrapText(true);
                //$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
                $objPHPExcel->getActiveSheet()->setTitle('Recapitulatif de la S-Direction');
                $objPHPExcel->setActiveSheetIndex(1);
                //table header
                $headerIndex = 8;
                $bodyIndex = 9;
                $objPHPExcel->setActiveSheetIndex(1)
                        ->setCellValue('A'.$headerIndex, 'Tâche')
                        ->setCellValue('B'.$headerIndex, 'Priorité')
                        ->setCellValue('C'.$headerIndex, 'Etat')
                        ->setCellValue('D'.$headerIndex, 'Date de début')
                        ->setCellValue('E'.$headerIndex, 'Date d\'échéance')
                        ->setCellValue('F'.$headerIndex, 'Pourcentage achevé')
                        ->setCellValue('G'.$headerIndex, 'Terminée/En Retard')
                        ->setCellValue('H'.$headerIndex, 'Notes')
                        ->setCellValue('I'.$headerIndex, 'Agents concernés');

                $objPHPExcel->getActiveSheet()->getStyle('A'.$headerIndex.':I'.$headerIndex)->applyFromArray($boldHeader);
                
                foreach ($points as $point)
                {
                    $point['expiryDate'] = implode('/', array_reverse(explode('-', $point['expiryDate'])));
                    $point['startDate'] = implode('/', array_reverse(explode('-', $point['startDate'])));
                    $point['endDate'] = implode('/', array_reverse(explode('-', $point['endDate'])));
                    
                    if($point['stateName'] == 'Terminé'){
                        $state = 'Terminé';
                    }elseif($point['expiryDate']<$point['endDate']){
                        $state = 'En Retard';
                    }else{
                        $state = '';
                    }

                    $pointAgents = $point['agents'][0];
                    for($i=1; $i< count($point['agents']); $i++){
                        $pointAgents .= "\n\n".$point['agents'][$i];
                    }
                    //table body
                    $objPHPExcel->setActiveSheetIndex(1)
                        ->setCellValue('A'.$bodyIndex, strtoupper($point['projectName'])." : ".$point['activity'])
                        ->setCellValue('B'.$bodyIndex, 'Elévé')
                        ->setCellValue('C'.$bodyIndex, $point['stateName'])
                        ->setCellValue('D'.$bodyIndex, $point['startDate'])
                        ->setCellValue('E'.$bodyIndex, $point['expiryDate']=='00/00/0000'?'-':$point['expiryDate'])
                        ->setCellValue('F'.$bodyIndex, $point['purcent'].'%')
                        ->setCellValue('G'.$bodyIndex, $state)
                        ->setCellValue('H'.$bodyIndex, $point['observation'])
                        ->setCellValue('I'.$bodyIndex, $pointAgents);

                    $objPHPExcel->getActiveSheet()->getStyle('A'.$bodyIndex)->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle('H'.$bodyIndex)->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle('I'.$bodyIndex)->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle('B'.$bodyIndex.':I'.$bodyIndex)->applyFromArray($center);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$bodyIndex)->applyFromArray($style);

                    $bodyIndex++;
                    
                }
                
                if(empty($params['dateDebut']) && empty($params['dateFin'])){
                    $dernierLundi = date('Y-m-d', strtotime('monday'))==date('Y-m-d') ? date('d/m/Y', strtotime('monday')) : date('d/m/Y',strtotime('last monday'));
                    $fileName = 'Point des activités des services de la semaine du '.$dernierLundi;
                }else{
                    $fileName = 'Point des activités des services du '.$params['dateDebut'].' au '.$params['dateFin'];
                }
                
                $objWriter = $this->phpExcel->createWriter($objPHPExcel, 'Excel2007' );

                $response = $this->phpExcel->createHttpResponse($objWriter, 200, [
                    'Pragma' => 'public',
                    'Cache-control' => 'must-revalidate, post-check=0, pre-check=0',
                    'Cache-control' => 'private',
                    'Expires' => '0000-00-00',
                    'Content-Type' => 'application/vnd.ms-excel; charset=utf-8',
                    'Content-Disposition' => 'attachment; filename=' . $fileName.'.xlsx',
                ]);
            }
            
        } catch (Exception $e) {
            $response->setStatusCode(500);
            echo $e->getTraceAsString();
        }
        return $response;
    }
    
}

