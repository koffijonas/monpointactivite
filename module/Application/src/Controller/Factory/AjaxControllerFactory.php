<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\AjaxController;
use Application\Service\AjaxManager;
use Application\Service\PointManager;

/**
 * This is the factory for IndexController. Its purpose is to instantiate the
 * controller and inject dependencies into it.
 */
class AjaxControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $ajaxManager = $container->get(AjaxManager::class);
        $pointManager = $container->get(PointManager::class);
        $phpExcel = $container->get('mvlabs.phpexcel.service');
        // Instantiate the controller and inject dependencies
        return new AjaxController($entityManager, $ajaxManager, $pointManager, $phpExcel);
    }
}