<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\PointController;
use Application\Service\PointManager;
use Application\Service\ProjectManager;

/**
 * This is the factory for IndexController. Its purpose is to instantiate the
 * controller and inject dependencies into it.
 */
class PointControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $pointManager = $container->get(PointManager::class);
        $projectManager = $container->get(ProjectManager::class);
        // Instantiate the controller and inject dependencies
        return new PointController($entityManager, $pointManager, $projectManager);
    }
}