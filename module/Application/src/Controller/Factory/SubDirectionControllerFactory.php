<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\SubDirectionController;
use Application\Service\SubDirectionManager;

/**
 * This is the factory for IndexController. Its purpose is to instantiate the
 * controller and inject dependencies into it.
 */
class SubDirectionControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $subDirectionManager = $container->get(SubDirectionManager::class);
        // Instantiate the controller and inject dependencies
        return new SubDirectionController($entityManager, $subDirectionManager);
    }
}