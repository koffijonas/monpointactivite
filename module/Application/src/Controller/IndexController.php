<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Entity\User;
use Application\Entity\Project;
use Application\Entity\Point;
use Application\Service\PointManager;

/**
 * This is the main controller class of the User Demo application. It contains
 * site-wide actions such as Home or About.
 */
class IndexController extends AbstractActionController 
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    
    /**
     * Ajax manager.
     * @var Application\Service\PointManager
     */
    private $pointManager;
    
    /**
     * Constructor. Its purpose is to inject dependencies into the controller.
     */
    public function __construct($entityManager, $pointManager) 
    {
       $this->entityManager = $entityManager;
       $this->pointManager = $pointManager;
    }
    
    /**
     * This is the default "index" action of the controller. It displays the 
     * Home page.
     */
    public function indexAction() 
    {
        // Nombre total de projet pour le service
        $userConnected = $this->currentUser();
        
        if($userConnected){
            $isSousDirecteur = $userConnected->getFunction()=='Sous-Directeur' ? true : false;
            $isChefDeService = $userConnected->getFunction()=='Chef de service' ? true : false;
            
            
            $projects = $this->pointManager->getNombreProjetByUser($userConnected);
            $finishedActivitiesTotal = $this->pointManager->getNbrActiviteEndByUser($userConnected);
            $notFinishedActivitiesTotal = $this->pointManager->getNbrActiviteNotEndByUser($userConnected);
            $serviceUsersTotal = $this->pointManager->getNbrCollaborateurs($userConnected);
            //$users = $this->entityManager->getRepository(User::class)->findBy(['service'=>$userConnected->getService()], ['dateCreated'=>'DESC']);
            //$serviceUsersTotal = count($users);

            return new ViewModel([
                'projectsTotal' => $projects,
                'finishedActivitiesTotal' => $finishedActivitiesTotal,
                'notFinishedActivitiesTotal' => $notFinishedActivitiesTotal,
                'serviceUsersTotal' => $serviceUsersTotal,
                'isSousDirecteur' => $isSousDirecteur,
                'isChefDeService' => $isChefDeService
            ]);
        }
    }

    /**
     * This is the "about" action. It is used to display the "About" page.
     */
    public function aboutAction() 
    {       
        // Return variables to view script with the help of
        // ViewObject variable container
        $view = new ViewModel();
        $view->setTerminal(true);
        
        return $view;
    }  
    
    /**
     * The "settings" action displays the info about currently logged in user.
     */
    public function settingsAction()
    {
        $id = $this->params()->fromRoute('id');
        
        if ($id!=null) {
            $user = $this->entityManager->getRepository(User::class)
                    ->find($id);
        } else {
            $user = $this->currentUser();
        }
        
        if ($user==null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        if (!$this->access('profile.any.view') && 
            !$this->access('profile.own.view', ['user'=>$user])) {
            return $this->redirect()->toRoute('not-authorized');
        }
        
        return new ViewModel([
            'user' => $user
        ]);
    }
}

