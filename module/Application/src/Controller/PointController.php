<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\Point;
use Application\Entity\Collaboration;
use Application\Entity\Project;
use Application\Entity\Service;
use User\Entity\User;
use Application\Entity\State;
use Application\Entity\Validation;
use Application\Form\Point\PointForm;
use Application\Form\Point\Recherche;

/**
 * This is the main controller class of the User Demo application. It contains
 * site-wide actions such as Home or About.
 */
class PointController extends AbstractActionController 
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    
     /**
     * Point manager.
     * @var Application\Service\PointManager 
     */
    private $pointManager;
    
    /**
     * Point manager.
     * @var Application\Service\ProjectManager 
     */
    private $projectManager;
    
    private $url;
    
    /**
     * Constructor. Its purpose is to inject dependencies into the controller.
     */
    public function __construct($entityManager, $pointManager, $projectManager) 
    {
       $this->entityManager = $entityManager;
       $this->pointManager = $pointManager;
       $this->projectManager = $projectManager;
    }
    
    public function indexAction() 
    {
        $form = new Recherche();
        
        $allStates = $this->entityManager->getRepository(State::class)
                ->findBy([], ['stateName'=>'ASC']);
        $statesList = [];
        foreach ($allStates as $state) {
            $statesList[$state->getId()] = $state->getStateName();
        }
        $form->get('executionState')->setValueOptions($statesList);
        
        if($this->getRequest()->isPost()){
            
            if($this->getRequest()->getPost('activiteId')){
                $data = $this->getRequest()->getPost();
                $pointId = $data['activiteId'];
                unset($data['activiteId']);
                
                foreach ($data as $key => $idTableCollab) {
                    if(substr($key, 0,1)=='_'){
                        $keyPrime = substr($key, 1);
                        $userId = $data[$keyPrime];
                        
                        if($idTableCollab=='null' && isset($userId) ){// Cas d'un ajout
                            $collabTable = new Collaboration();
                            $collabTable->setUserId($userId);
                            $collabTable->setPointId($pointId);
                            $this->entityManager->persist($collabTable);
                            $this->entityManager->flush();
                            
                        }else if($idTableCollab!='null' && !isset($userId) ){//Cas d'une suppression
                            $collabLine = $this->entityManager->getRepository(Collaboration::class)->find($idTableCollab);
                            $this->entityManager->remove($collabLine);
                            $this->entityManager->flush();
                        }
                    }
                }
                
                return $this->redirect()->toRoute('pointactivites', ['action'=>'index']);
            }
            
            $params['dateDebut'] = $this->getRequest()->getPost('start');
            $params['dateFin'] = $this->getRequest()->getPost('end');
            $params['executionState'] = $this->getRequest()->getPost('executionState');
            $form->populateValues([
                'start' => $params['dateDebut'],
                'end' => $params['dateFin'],
                'executionState' => $params['executionState'],
            ]);
            
        }
        
        $dernierLundi = date('Y-m-d', strtotime('monday'))==date('Y-m-d') ? date('d/m/Y', strtotime('monday')) : date('d/m/Y',strtotime('last monday'));
        //$this->view->dernierLundi = $dernierLundi;
        
        return new ViewModel([
            'form' => $form,
            'dernierLundi' => $dernierLundi,
        ]);
    }
    
    public function addAction() 
    {
      $form = new PointForm('create', $this->entityManager);
      $user = $this->currentUser();
      $allProjects = $this->entityManager->getRepository(Project::class)
                ->findBy(['serviceId'=>$user->getService()], ['projectName'=>'ASC']);
        foreach ($allProjects as $project) {
            $projectList[$project->getId()] = $project->getProjectName();
        }
        
        $projectList['autre'] = 'Autre';
        
        $form->get('domaine')->setValueOptions($projectList);
        
        $allStates = $this->entityManager->getRepository(State::class)
                ->findBy([], ['stateName'=>'ASC']);
        foreach ($allStates as $state) {
            $statesList[$state->getId()] = $state->getStateName();
        }
        $form->get('executionState')->setValueOptions($statesList);
        
        //Clique sur retour
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('annuler')){
            return $this->redirect()->toRoute('pointactivites', 
                        ['action'=>'index']);
        }
        
        // Clique sur enregistrer
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('enregistrer')) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                // Get filtered and validated data
                $data = $form->getData();
                /*if($data['executionState']==3 && $data['endDate']==''){
                    // Add a flash message.
                    $this->flashMessenger()->addErrorMessage('Lorqu\'un point est terminé, vous devez ajouter la date de fin');

                    // Redirect to "index" page
                    return $this->redirect()->toRoute('pointactivites', ['action'=>'add']);                
                         
                }
                
                if($data['startDate'] > $data['endDate']){
                    // Add a flash message.
                    $this->flashMessenger()->addErrorMessage('La date de fin doit être supérieure à celle de début');

                    // Redirect to "index" page
                    return $this->redirect()->toRoute('pointactivites', ['action'=>'add']);                
                         
                }*/
                
                if($data['domaine']=='autre'){
                    $anotherDomaine = array(
                        'projectName'=>$data['anotherDomaine'],
                        'service'=>$user->getService(),
                    );
                    $domaine = $this->projectManager->addProject($anotherDomaine);
                    $data['domaine'] = $domaine->getId();
                }
                //var_dump($data);die;
                // Add user.
                $point = $this->pointManager->addPoint($data, $user);
                
                // Redirect to "view" page
                return $this->redirect()->toRoute('pointactivites', 
                        ['action'=>'index']);                
            }               
        } 
        
        return new ViewModel([
            'form' => $form
        ]);
    }
    
    public function editAction() 
    {
        
        $this->url = $this->getRequest()->getHeader('Referer')->getUri();
        
        
        
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $point = $this->entityManager->getRepository(Point::class)
                ->find($id);
        
        if ($point == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
     
        $form = new PointForm('update', $this->entityManager, $point);
        $user = $this->currentUser();
        $allProjects = $this->entityManager->getRepository(Project::class)
                  ->findBy(['serviceId'=>$user->getService()], ['projectName'=>'ASC']);
        $projectList = [];
        foreach ($allProjects as $project) {
            $projectList[$project->getId()] = $project->getProjectName();
        }

        $projectList['autre'] = 'Autre';

        $form->get('domaine')->setValueOptions($projectList);

        $allStates = $this->entityManager->getRepository(State::class)
              ->findBy([], ['stateName'=>'ASC']);
        $StatesList = [];
        foreach ($allStates as $state) {
            $StatesList[$state->getId()] = $state->getStateName();
        }
        $form->get('executionState')->setValueOptions($StatesList);
        
        //Clique sur retour
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('annuler')){
            $data = $this->params()->fromPost();
            
            return $this->redirect()->toUrl($data['returnUrl']);
        }
        
        // Clique sur enregistrer
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('enregistrer')) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            //var_dump($data); die;
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                // Get filtered and validated data
                $data = $form->getData();
                
                if($data['domaine']=='autre'){
                    $anotherDomaine = array(
                        'projectName'=>$data['anotherDomaine'],
                        'service'=>$user->getService(),
                    );
                    $domaine = $this->projectManager->addProject($anotherDomaine);
                    $data['domaine'] = $domaine->getId();
                }
                // Add user.
                $point = $this->pointManager->updatePoint($point, $data);
                
                return $this->redirect()->toUrl($data['returnUrl']);                
            }               
        }else {
            $form->setData(array(
                'domaine'=>$point->getDomaine(),
                'activity'=>$point->getActivity(),
                'expiryDate'=>$point->getExpiryDate(),
                'startDate'=>$point->getStartDate(), 
                'endDate' => $point->getEndDate(),
                'executionState' => $point->getExecutionState(),
                'purcent' => $point->getPurcent(),
                'observation' => $point->getObservation(),
                'returnUrl' => $this->url,
            ));
        }
        
        return new ViewModel([
            'form' => $form

        ]);
    }
    
    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $point = $this->entityManager->getRepository(Point::class)
                ->find($id);
        
        if ($point == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        $userConnected = $this->currentUser();
        if($userConnected->getId() == $point->getUserId()){
            // Delete point and collaborators.
            $this->pointManager->deletePoint($point);
        }else{
            // Delete the collaboration of this user.
            $thisCollaboratorion = $this->entityManager->getRepository(Collaboration::class)
                                ->findOneBy(['pointId'=> $point->getId(), 'userId'=> $userConnected->getId()]);
            $this->entityManager->remove($thisCollaboratorion);
            $this->entityManager->flush();
        }
            
        
        // Add a flash message.
        $this->flashMessenger()->addSuccessMessage('Point supprimé avec succès.');

        // Redirect to "index" page
        return $this->redirect()->toRoute('pointactivites', ['action'=>'index']); 
    }
    
    public function pointsDeMesAgentsAction() 
    {
        //var_dump( $this->getRequest()->getPost() ); die;
        //$date = new Zend\D
        //var_dump($date); die;
        $form = new Recherche('chef'); 
        
        $user = $this->currentUser();
        $myServiceUsers = $this->entityManager->getRepository(User::class)
                ->findBy(['service'=>$user->getService()], ['fullName'=>'ASC']);
        $myServiceUsersList = [];
        foreach ($myServiceUsers as $myServiceUser) {
            $myServiceUsersList[$myServiceUser->getId()] = $myServiceUser->getFullName();
        }
        $form->get('agent')->setValueOptions($myServiceUsersList);
        
        $allStates = $this->entityManager->getRepository(State::class)
                ->findBy([], ['stateName'=>'ASC']);
        $statesList = [];
        foreach ($allStates as $state) {
            $statesList[$state->getId()] = $state->getStateName();
        }
        $form->get('executionState')->setValueOptions($statesList);
        
        $params = [
            'user'=>$user
        ];
        if($this->getRequest()->isPost()){
            $params['agent'] = $this->getRequest()->getPost('agent');
            $params['dateDebut'] = $this->getRequest()->getPost('start');

            $params['dateFin'] = $this->getRequest()->getPost('end');
            $params['executionState'] = $this->getRequest()->getPost('executionState');

            $form->populateValues([
                'start' => $params['dateDebut'],
                'end' => $params['dateFin'],
                'agent' => $params['agent'],
                'executionState' => $params['executionState'],
            ]);
            if($this->getRequest()->getPost('rechercher')){
                
            }
            else{
                $this->pointManager->validatePointAgents($params);
                $this->flashMessenger()->addSuccessMessage('Point validé avec succès.');
                return $this->redirect()->toRoute('pointactivites', ['action'=>'pointsDeMesAgents']); 
            }
            
        }
        
        $resultat = $this->pointManager->getActivitesDesAgents($params);
        
        // get Validation status
        
        $query = $this->entityManager->createQueryBuilder();
        
        $query->select(['v.pointId', 'v.validationState'])
                ->from(Validation::class, 'v')
                ;
        
        if(empty($params['dateDebut']) && empty($params['dateFin']) && empty($params['executionState'])){
            $dernierLundi = date('Y-m-d', strtotime('monday'))==date('Y-m-d') ? date('Y-m-d', strtotime('monday')) : date('Y-m-d',strtotime('last monday'));
           
            $query->where('v.validationDate >= :dd')->setParameter('dd', $dernierLundi);
            
        }else if(!empty($params['dateDebut']) && !empty($params['dateFin'])){
            //$query->andWhere( $query->expr()->between('a.startDate', $params['dateDebut'], $params['dateFin']) );
            $query->andWhere('(v.validationDate >= ?1')->setParameter(1, $params['dateDebut']);
            $query->andWhere('v.validationDate <= ?2)')->setParameter(2, $params['dateFin']);
            
            
        }else{
            if(isset($params['dateDebut']) && !empty($params['dateDebut'])){
                $query->andWhere('v.validationDate >= ?1')->setParameter(1, $params['dateDebut']);
            }

            if(isset($params['dateFin']) && !empty($params['dateFin'])){
                $query->andWhere('v.validationDate <= ?3')->setParameter(3, $params['dateFin']);
            }
        }
        
        $results = $query->getQuery()->getResult();
        $allValidationsForThisWeek = [];
        foreach ($results as $res){
            $allValidationsForThisWeek[$res['pointId']] = $res['validationState'];
        }
        
        $dernierLundi = date('Y-m-d', strtotime('monday'))==date('Y-m-d') ? date('d/m/Y', strtotime('monday')) : date('d/m/Y',strtotime('last monday'));
        
        //var_dump($resultat);die;	         
        return new ViewModel([
            'form' => $form,
            'pointsList' => $resultat,
            'myServiceUsers' => $myServiceUsers,
            'validation' => $allValidationsForThisWeek,
            'dernierLundi' => $dernierLundi
        ]);
    }
    
    public function pointsDeMesServicesAction() 
    {
        //var_dump( $this->getRequest()->getPost() ); die;
        //$date = new Zend\D
        //var_dump($date); die;
        $form = new Recherche('sous-directeur'); 
        
        $user = $this->currentUser();
        $mySubDirectionServices = $this->entityManager->getRepository(Service::class)
                ->findBy(['subDirectionId'=>$user->getService()], ['serviceName'=>'ASC']);
        $mySubDirectionServicesList = [];
        foreach ($mySubDirectionServices as $mySubDirectionService) {
            $mySubDirectionServicesList[$mySubDirectionService->getId()] = $mySubDirectionService->getServiceName();
        }
        $form->get('service')->setValueOptions($mySubDirectionServicesList);
        
        $allStates = $this->entityManager->getRepository(State::class)
                ->findBy([], ['stateName'=>'ASC']);
        $statesList = [];
        foreach ($allStates as $state) {
            $statesList[$state->getId()] = $state->getStateName();
        }
        $form->get('executionState')->setValueOptions($statesList);
        
        $params = [
            'user'=>$user
        ];
        if($this->getRequest()->isPost()){
            $params['service'] = $this->getRequest()->getPost('service');
            $params['dateDebut'] = $this->getRequest()->getPost('start');
            
            $params['dateFin'] = $this->getRequest()->getPost('end');
            $params['executionState'] = $this->getRequest()->getPost('executionState');
            
            $form->populateValues([
                'start' => $params['dateDebut'],
                'end' => $params['dateFin'],
                'service' => $params['service'],
                'executionState' => $params['executionState'],
            ]);
        }
        
        $resultats = $this->pointManager->getActivitesDesServices($params);
        $pointAgents = array();
        $allPoints = array();
        $pointIds = array();
        foreach($resultats as $point)
        {
          $pointAgents = [];
          if(!in_array($point['id'], $pointIds)){
            foreach($resultats as $res)
            {
              if($point['id'] == $res['id']) {
                 $pointAgents[] = $res['fullName'];   
              }
            }
            $point['agents'] = $pointAgents;
            $allPoints[] = $point;
            $pointIds[] = $point['id'];
          }
          
        }
        // get Validation status
        
        $query = $this->entityManager->createQueryBuilder();
        
        $query->select(['v.pointId', 'v.validationState'])
                ->from(Validation::class, 'v')
                ;
        
        if(empty($params['dateDebut']) && empty($params['dateFin'])){
            $dernierLundi = date('Y-m-d', strtotime('monday'))==date('Y-m-d') ? date('Y-m-d', strtotime('monday')) : date('Y-m-d',strtotime('last monday'));
           
            $query->where('v.validationDate >= :dd')->setParameter('dd', $dernierLundi);
            
        }else if(!empty($params['dateDebut']) && !empty($params['dateFin'])){
            //$query->andWhere( $query->expr()->between('a.startDate', $params['dateDebut'], $params['dateFin']) );
            $query->andWhere('(v.validationDate >= ?1')->setParameter(1, $params['dateDebut']);
            $query->andWhere('v.validationDate <= ?2)')->setParameter(2, $params['dateFin']);
            
            
        }else{
            if(isset($params['dateDebut']) && !empty($params['dateDebut'])){
                $query->andWhere('v.validationDate >= ?1')->setParameter(1, $params['dateDebut']);
            }

            if(isset($params['dateFin']) && !empty($params['dateFin'])){
                $query->andWhere('v.validationDate <= ?3')->setParameter(3, $params['dateFin']);
            }
        }
        
        $results = $query->getQuery()->getResult();
        $allValidationsForThisWeek = [];
        foreach ($results as $res){
            $allValidationsForThisWeek[$res['pointId']] = $res['validationState'];
        }
        
        $dernierLundi = date('Y-m-d', strtotime('monday'))==date('Y-m-d') ? date('d/m/Y', strtotime('monday')) : date('d/m/Y',strtotime('last monday'));
        
        return new ViewModel([
            'form' => $form,
            'pointsList' => $allPoints,
            'mySubDirectionServices' => $mySubDirectionServices,
            'validation' => $allValidationsForThisWeek,
            'dernierLundi' => $dernierLundi
        ]);
    }

}

