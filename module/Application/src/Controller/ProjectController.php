<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\Project;
use Application\Entity\Service;
use Application\Form\Project\ProjectForm;

/**
 * This is the main controller class of the User Demo application. It contains
 * site-wide actions such as Home or About.
 */
class ProjectController extends AbstractActionController 
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    
     /**
     * Project manager.
     * @var Application\Service\ProjectManager 
     */
    private $projectManager;
    
    private $adminCheck;


    /**
     * Constructor. Its purpose is to inject dependencies into the controller.
     */
    public function __construct($entityManager, $projectManager) 
    {
       $this->entityManager = $entityManager;
       $this->projectManager = $projectManager;
    }
    
    public function indexAction() 
    {  
        $allServices = $this->entityManager->getRepository(Service::class)
                ->findBy([], ['serviceName'=>'ASC']);
        $serviceList = [];
        foreach ($allServices as $service) {
            $serviceList[$service->getId()] = $service->getServiceName();
        }
        
        $user = $this->currentUser();
        $userRoles = $user->getRoles();
        foreach ($userRoles as $userRole){
            if($userRole->getId() == 1){
                $this->adminCheck = true;
            }
            else{
                $this->adminCheck = false;
            }
        }
        if ($this->adminCheck){
            $projects = $this->entityManager->getRepository(Project::class)
                ->findBy([], ['addDate'=>'DESC']);
        }
        else{
            $projects = $this->entityManager->getRepository(Project::class)
                ->findBy(['serviceId'=>$user->getService()], ['addDate'=>'DESC']);
        }
        

        
        return new ViewModel([
                'projects' => $projects,
                'services' => $serviceList
            ]);
    }
    
    public function addAction() 
    {
        $form = new ProjectForm('create', $this->entityManager);
        
        $allServices = $this->entityManager->getRepository(Service::class)
                ->findBy([], ['serviceName'=>'ASC']);
        $serviceList = [];
        foreach ($allServices as $service) {
            $serviceList[$service->getId()] = $service->getServiceName();
        }
        
        $form->get('service')->setValueOptions($serviceList);
        
        $user = $this->currentUser();
        $userRoles = $user->getRoles();
        foreach ($userRoles as $userRole){
            if($userRole->getId() == 1){
                $this->adminCheck = true;
            }
            else{
                $this->adminCheck = false;
            }
        }
        // Get the list of all available roles (sorted by name).
        if ($this->adminCheck){
            
        }
        else{
            $form->setData(array(
                'service'=>$user->getService()
            ));
            
            $form->get('service')->setAttributes(array(
                'disabled' => 'disabled',  
            ));
        }
        
        //Clique sur retour
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('annuler')){
            return $this->redirect()->toRoute('projects', 
                        ['action'=>'index']);
        }
        
        // Clique sur enregistrer
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('enregistrer')) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()){
                
                // Get filtered and validated data
                $data = $form->getData();
                
                // Add user.
                $project = $this->projectManager->addProject($data);
                
                // Redirect to "view" page
                return $this->redirect()->toRoute('projects', 
                        ['action'=>'index']);                
            }               
        } 
        
        return new ViewModel([
                'form' => $form
        ]);
    }
    
    public function editAction() 
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $project = $this->entityManager->getRepository(Project::class)
                ->find($id);
        
        if ($project == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
     
        $form = new ProjectForm('update', $this->entityManager, $project);
        
        $allServices = $this->entityManager->getRepository(Service::class)
                ->findBy([], ['serviceName'=>'ASC']);
        $serviceList = [];
        foreach ($allServices as $service) {
            $serviceList[$service->getId()] = $service->getServiceName();
        }
        
        $form->get('service')->setValueOptions($serviceList);
        $user = $this->currentUser();
        $userRoles = $user->getRoles();
        foreach ($userRoles as $userRole){
            if($userRole->getId() == 1){
                $this->adminCheck = true;
            }
            else{
                $this->adminCheck = false;
            }
        }
        // Get the list of all available roles (sorted by name).
        if ($this->adminCheck){
            
        }
        else{
            $form->get('service')->setAttributes(array(
                'disabled' => 'disabled',  
            ));
        }
        //Clique sur retour
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('annuler')){
            return $this->redirect()->toRoute('projects', 
                        ['action'=>'index']);
        }
        
        // Clique sur enregistrer
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('enregistrer')) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                // Get filtered and validated data
                $data = $form->getData();
                
                // Add user.
                $project = $this->projectManager->updateProject($project, $data);
                
                // Redirect to "view" page
                return $this->redirect()->toRoute('projects', 
                        ['action'=>'index']);                
            }               
        }else {
            
            $form->setData(array(
                    'projectName'=>$project->getProjectName(),
                    'service'=>$project->getServiceId(),
                ));
        }
        
        return new ViewModel([
                'form' => $form
                
            ]);
    }
    
    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $project = $this->entityManager->getRepository(Project::class)
                ->find($id);
        
        if ($project == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        // Delete role.
        $this->projectManager->deleteProject($project);
        
        // Add a flash message.
        $this->flashMessenger()->addSuccessMessage('Project supprimé avec succès.');

        // Redirect to "index" page
        return $this->redirect()->toRoute('projects', ['action'=>'index']); 
    }

}

