<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\Service;
use Application\Entity\SubDirection;
use Application\Form\Service\ServiceForm;

/**
 * This is the main controller class of the User Demo application. It contains
 * site-wide actions such as Home or About.
 */
class ServiceController extends AbstractActionController 
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    
     /**
     * Service manager.
     * @var Application\Service\ServiceManager 
     */
    private $serviceManager;
    
    /**
     * Constructor. Its purpose is to inject dependencies into the controller.
     */
    public function __construct($entityManager, $serviceManager) 
    {
       $this->entityManager = $entityManager;
       $this->serviceManager = $serviceManager;
    }
    
    public function indexAction() 
    {
        // Access control.
        if (!$this->access('service.manage')) {
            $this->getResponse()->setStatusCode(401);
            return;
        }
        
        $allSubDirections = $this->entityManager->getRepository(SubDirection::class)
                ->findBy([], ['libSousDirection'=>'ASC']);
        $subDirectionList = [];
        foreach ($allSubDirections as $subDirection){
            $subDirectionList[$subDirection->getId()] = $subDirection->getlibSousDirection();
        }
        
        $services = $this->entityManager->getRepository(Service::class)
                ->findBy([], ['addDate'=>'DESC']);
        return new ViewModel([
                'services' => $services,
                'subDirection' => $subDirectionList
            ]);
    }
    
    public function addAction() 
    {
        $form = new ServiceForm('create', $this->entityManager);
        
        $allSubDirections = $this->entityManager->getRepository(SubDirection::class)
                ->findBy([], ['libSousDirection'=>'ASC']);
        $subDirectionList = [];
        foreach ($allSubDirections as $subDirection){
            $subDirectionList[$subDirection->getId()] = $subDirection->getlibSousDirection();
        }
        
        $form->get('subDirection')->setValueOptions($subDirectionList);
        
        //Clique sur retour
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('annuler')){
            return $this->redirect()->toRoute('services', 
                        ['action'=>'index']);
        }
        
        // Clique sur enregistrer
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('enregistrer')) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                // Get filtered and validated data
                $data = $form->getData();
                
                // Add user.
                $service = $this->serviceManager->addService($data);
                
                // Redirect to "view" page
                return $this->redirect()->toRoute('services', 
                        ['action'=>'index']);                
            }               
        } 
        
        return new ViewModel([
                'form' => $form
            ]);
    }
    
    public function editAction() 
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $service = $this->entityManager->getRepository(Service::class)
                ->find($id);
        
        if ($service == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
     
        $form = new ServiceForm('update', $this->entityManager, $service);
        
        $allSubDirections = $this->entityManager->getRepository(SubDirection::class)
                ->findBy([], ['libSousDirection'=>'ASC']);
        $subDirectionList = [];
        foreach ($allSubDirections as $subDirection){
            $subDirectionList[$subDirection->getId()] = $subDirection->getlibSousDirection();
        }
        
        $form->get('subDirection')->setValueOptions($subDirectionList);
        //Clique sur retour
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('annuler')){
            return $this->redirect()->toRoute('services', 
                        ['action'=>'index']);
        }
        
        // Clique sur enregistrer
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('enregistrer')) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                // Get filtered and validated data
                $data = $form->getData();
                
                // Add user.
                $service = $this->serviceManager->updateService($service, $data);
                
                // Redirect to "view" page
                return $this->redirect()->toRoute('services', 
                        ['action'=>'index']);                
            }               
        }else {
            
            $form->setData(array(
                    'serviceName'=>$service->getServiceName(),
                    'subDirection'=>$service->getSubDirectionId()
                ));
        }
        
        return new ViewModel([
                'form' => $form
                
            ]);
    }
    
    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $service = $this->entityManager->getRepository(Service::class)
                ->find($id);
        
        if ($service == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        // Delete role.
        $this->serviceManager->deleteService($service);
        
        // Add a flash message.
        $this->flashMessenger()->addSuccessMessage('Service supprimé avec succès.');

        // Redirect to "index" page
        return $this->redirect()->toRoute('services', ['action'=>'index']); 
    }

}

