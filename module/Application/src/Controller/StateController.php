<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\State;
use Application\Form\State\StateForm;

/**
 * This is the main controller class of the User Demo application. It contains
 * site-wide actions such as Home or About.
 */
class StateController extends AbstractActionController 
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    
     /**
     * State manager.
     * @var Application\Service\StateManager 
     */
    private $stateManager;
    
    /**
     * Constructor. Its purpose is to inject dependencies into the controller.
     */
    public function __construct($entityManager, $stateManager) 
    {
       $this->entityManager = $entityManager;
       $this->stateManager = $stateManager;
    }
    
    public function indexAction() 
    {
        
        $states = $this->entityManager->getRepository(State::class)
                ->findBy([], ['addDate'=>'DESC']);
        return new ViewModel([
                'states' => $states,
            ]);
    }
    
    public function addAction() 
    {
     $form = new StateForm('create', $this->entityManager);
        
        //Clique sur retour
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('annuler')){
            return $this->redirect()->toRoute('states', 
                        ['action'=>'index']);
        }
        
        // Clique sur enregistrer
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('enregistrer')) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                // Get filtered and validated data
                $data = $form->getData();
                
                // Add user.
                $state = $this->stateManager->addState($data);
                
                // Redirect to "view" page
                return $this->redirect()->toRoute('states', 
                        ['action'=>'index']);                
            }               
        } 
        
        return new ViewModel([
                'form' => $form
            ]);
    }
    
    public function editAction() 
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $state = $this->entityManager->getRepository(State::class)
                ->find($id);
        
        if ($state == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
     
        $form = new StateForm('update', $this->entityManager, $state);
        
        //Clique sur retour
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('annuler')){
            return $this->redirect()->toRoute('states', 
                        ['action'=>'index']);
        }
        
        // Clique sur enregistrer
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('enregistrer')) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                // Get filtered and validated data
                $data = $form->getData();
                
                // Add user.
                $state = $this->stateManager->updateState($state, $data);
                
                // Redirect to "view" page
                return $this->redirect()->toRoute('states', 
                        ['action'=>'index']);                
            }               
        }else {
            
            $form->setData(array(
                    'stateName'=>$state->getStateName(),
                ));
        }
        
        return new ViewModel([
                'form' => $form
                
            ]);
    }
    
    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $state = $this->entityManager->getRepository(State::class)
                ->find($id);
        
        if ($state == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        // Delete role.
        $this->stateManager->deleteState($state);
        
        // Add a flash message.
        $this->flashMessenger()->addSuccessMessage('Statut supprimé avec succès.');

        // Redirect to "index" page
        return $this->redirect()->toRoute('states', ['action'=>'index']); 
    }

}

