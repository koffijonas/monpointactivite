<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\SubDirection;
use Application\Form\SubDirection\DirectionForm;

/**
 * This is the main controller class of the User Demo application. It contains
 * site-wide actions such as Home or About.
 */
class SubDirectionController extends AbstractActionController 
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    
     /**
     * SubDirection manager.
     * @var Application\Service\SubDirectionManager 
     */
    private $subDirectionManager;
    
    /**
     * Constructor. Its purpose is to inject dependencies into the controller.
     */
    public function __construct($entityManager, $subDirectionManager) 
    {
       $this->entityManager = $entityManager;
       $this->subDirectionManager = $subDirectionManager;
    }
    
    public function indexAction() 
    {
        if (!$this->access('subDirection.manage')) {
            $this->getResponse()->setStatusCode(401);
            return;
        }
        $subDirections = $this->entityManager->getRepository(SubDirection::class)
                ->findBy([], ['addDate'=>'DESC']);
        return new ViewModel([
                'subDirections' => $subDirections,
            ]);
    }
    
    public function addAction() 
    {
     $form = new DirectionForm('create', $this->entityManager);
        
        //Clique sur retour
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('annuler')){
            return $this->redirect()->toRoute('subDirections', 
                        ['action'=>'index']);
        }
        
        // Clique sur enregistrer
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('enregistrer')) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                // Get filtered and validated data
                $data = $form->getData();
                
                // Add user.
                $subDirection = $this->subDirectionManager->addSubDirection($data);
                
                // Redirect to "view" page
                return $this->redirect()->toRoute('subDirections', 
                        ['action'=>'index']);                
            }               
        } 
        
        return new ViewModel([
                'form' => $form
            ]);
    }
    
    public function editAction() 
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $subDirection = $this->entityManager->getRepository(SubDirection::class)
                ->find($id);
        
        if ($subDirection == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
     
        $form = new DirectionForm('update', $this->entityManager, $subDirection);
        
        //Clique sur retour
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('annuler')){
            return $this->redirect()->toRoute('subDirections', 
                        ['action'=>'index']);
        }
        
        // Clique sur enregistrer
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('enregistrer')) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                // Get filtered and validated data
                $data = $form->getData();
                
                // Add user.
                $subDirection = $this->subDirectionManager->updateSubDirection($subDirection, $data);
                
                // Redirect to "view" page
                return $this->redirect()->toRoute('subDirections', 
                        ['action'=>'index']);                
            }               
        }else {
            
            $form->setData(array(
                    'subDirectionName'=>$subDirection->getLibSousDirection(),
                    'subDirectionSigle'=>$subDirection->getSigleSousDirection(),
                ));
        }
        
        return new ViewModel([
                'form' => $form
                
            ]);
    }
    
    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $subDirection = $this->entityManager->getRepository(SubDirection::class)
                ->find($id);
        
        if ($subDirection == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        // Delete role.
        $this->subDirectionManager->deleteSubDirection($subDirection);
        
        // Add a flash message.
        $this->flashMessenger()->addSuccessMessage('Sous-Direction supprimé avec succès.');

        // Redirect to "index" page
        return $this->redirect()->toRoute('subDirections', ['action'=>'index']); 
    }

}

