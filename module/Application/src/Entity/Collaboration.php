<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a product.
 * @ORM\Entity()
 * @ORM\Table(name="collaboration_sur_point")
 */
class Collaboration
{
    /**
     * @ORM\id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(name="pointId")
     */
    protected $pointId;
    
    /**
     * @ORM\Column(name="userId")
     */
    private $userId;
    
    
    function getId() {
        return $this->id;
    }

    function getPointId() {
        return $this->pointId;
    }

    function getUserId() {
        return $this->userId;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPointId($pointId) {
        $this->pointId = $pointId;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }

}



