<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * This class represents a product.
 * @ORM\Entity()
 * @ORM\Table(name="pointactivites")
 */
class Point
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue
     */
    protected $id;
    
    /**
     * @ORM\Column(name="domaine")
     */
    protected $domaine;
    
    /**
     * @ORM\Column(name="activite")
     */
    protected $activity;
    
    /**
     * @ORM\Column(name="dateEcheance")
     */
    protected $expiryDate;

    /**
     * @ORM\Column(name="dateDebut")
     */
    protected $startDate;

    /**
     * @ORM\Column(name="dateFin")
     */
    private $endDate;
    
    /**
     * @ORM\Column(name="etatExecution")
     */
    private $executionState;
    
    /**
     * @ORM\Column(name="dateAjout")
     */
    private $addDate;
    
    /**
     * @ORM\Column(name="pourcentage")
     */
    private $purcent;
    
    /**
     * @ORM\Column(name="observation")
     */
    private $observation;
    
     /**
     * @ORM\Column(name="userId")
     */
    protected $userId;
    
    /**
     * @ORM\ManyToMany(targetEntity="User\Entity\User")
     * @ORM\JoinTable(name="collaboration_sur_point",
     *      joinColumns={@ORM\JoinColumn(name="pointId", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="userId", referencedColumnName="id")}
     *      )
     */
    private $collaborators;
    
    /**
     * Constructor.
     */
    public function __construct() 
    {
        $this->collaborators = new ArrayCollection();
    }
    
    
    function getId() {
        return $this->id;
    }

    function getDomaine() {
        return $this->domaine;
    }

    function getActivity() {
        return $this->activity;
    }
    
    function getExpiryDate() {
        return $this->expiryDate;
    }

    function getStartDate() {
        return $this->startDate;
    }

    function getEndDate() {
        return $this->endDate;
    }

    function getExecutionState() {
        return $this->executionState;
    }

    function getAddDate() {
        return $this->addDate;
    }
    
    function getPurcent() {
        return $this->purcent;
    }

    function getUserId() {
        return $this->userId;
    }
    
    function getObservation() {
        return $this->observation;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDomaine($domaine) {
        $this->domaine = $domaine;
    }

    function setActivity($activity) {
        $this->activity = $activity;
    }
    
    function setExpiryDate($expiryDate) {
        $this->expiryDate = $expiryDate;
    }

    function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    function setExecutionState($executionState) {
        $this->executionState = $executionState;
    }

    function setAddDate($addDate) {
        $this->addDate = $addDate;
    }
    
    function setPurcent($purcent) {
        $this->purcent = $purcent;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }

    function setObservation($observation) {
        $this->observation = $observation;
    }

    function getCollaborators() {
        return $this->collaborators;
    }

    function setCollaborators($collaborators) {
        $this->collaborators = $collaborators;
    }
    
    function toArray(){
        return get_object_vars($this);
    }
    
    /**
     * Assigns a role to user.
     */
    public function addCollaborator($collaborator)
    {
        $this->collaborators->add($collaborator);
    }

}



