<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a product.
 * @ORM\Entity()
 * @ORM\Table(name="projet")
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\Column(name="Id")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(name="libProjet")
     */
    protected $projectName;
    
    /**
     * @ORM\Column(name="idService")
     */
    protected $serviceId;
    
    /**
     * @ORM\Column(name="dateAjout")
     */
    private $addDate;
    
     /**
     * @ORM\Column(name="userId")
     */
    protected $userId;
    
    
    function getId() {
        return $this->id;
    }
    
    function getServiceId() {
        return $this->serviceId;
    }
    
    function getProjectName() {
        return $this->projectName;
    }

    function getAddDate() {
        return $this->addDate;
    }

    function getUserId() {
        return $this->userId;
    }

    function setId($id) {
        $this->id = $id;
    }
    
    function setServiceId($serviceId) {
        $this->serviceId = $serviceId;
    }

    function setProjectName($projectName) {
        $this->projectName = $projectName;
    }

    function setAddDate($addDate) {
        $this->addDate = $addDate;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }

    
    function toArray(){
        return get_object_vars($this);
    }
}



