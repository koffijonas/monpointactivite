<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a product.
 * @ORM\Entity()
 * @ORM\Table(name="service")
 */
class Service
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(name="libService")
     */
    protected $serviceName;
    
    /**
     * @ORM\Column(name="idSousDirection")
     */
    protected $subDirectionId;
    
    /**
     * @ORM\Column(name="dateAjout")
     */
    private $addDate;
    
     /**
     * @ORM\Column(name="userId")
     */
    protected $userId;
    
    
    function getId() {
        return $this->id;
    }

    function getServiceName() {
        return $this->serviceName;
    }

    function getSubDirectionId() {
        return $this->subDirectionId;
    }

    function getAddDate() {
        return $this->addDate;
    }

    function getUserId() {
        return $this->userId;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setServiceName($serviceName) {
        $this->serviceName = $serviceName;
    }

    function setSubDirectionId($subDirectionId) {
        $this->subDirectionId = $subDirectionId;
    }

    function setAddDate($addDate) {
        $this->addDate = $addDate;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }
    
    function toArray(){
        return get_object_vars($this);
    }

}



