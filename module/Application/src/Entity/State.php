<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a product.
 * @ORM\Entity()
 * @ORM\Table(name="statutActivite")
 */
class State
{
    /**
     * @ORM\Id
     * @ORM\Column(name="Id")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(name="libStatut")
     */
    protected $stateName;
    
    /**
     * @ORM\Column(name="dateAjout")
     */
    private $addDate;
    
     /**
     * @ORM\Column(name="userId")
     */
    protected $userId;
    
    
    function getId() {
        return $this->id;
    }

    function getStateName() {
        return $this->stateName;
    }

    function getAddDate() {
        return $this->addDate;
    }

    function getUserId() {
        return $this->userId;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setStateName($stateName) {
        $this->stateName = $stateName;
    }

    function setAddDate($addDate) {
        $this->addDate = $addDate;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }

}



