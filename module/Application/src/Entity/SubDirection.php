<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a product.
 * @ORM\Entity()
 * @ORM\Table(name="sous_direction")
 */
class SubDirection
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue
     */
    protected $id;
    
    /**
     * @ORM\Column(name="sigleSousDirection")
     */
    protected $sigleSousDirection;
    
    /**
     * @ORM\Column(name="libSousDirection")
     */
    protected $libSousDirection;
    
    /**
     * @ORM\Column(name="dateAjout")
     */
    private $addDate;
    
     /**
     * @ORM\Column(name="userId")
     */
    protected $userId;
    
    
    function getId() {
        return $this->id;
    }

    function getSigleSousDirection() {
        return $this->sigleSousDirection;
    }

    function getLibSousDirection() {
        return $this->libSousDirection;
    }

    function getAddDate() {
        return $this->addDate;
    }

    function getUserId() {
        return $this->userId;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setSigleSousDirection($sigleSousDirection) {
        $this->sigleSousDirection = $sigleSousDirection;
    }

    function setLibSousDirection($libSousDirection) {
        $this->libSousDirection = $libSousDirection;
    }

    function setAddDate($addDate) {
        $this->addDate = $addDate;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }
    
    function toArray(){
        return get_object_vars($this);
    }


}



