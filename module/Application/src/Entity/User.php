<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * This class represents a product.
 * @ORM\Entity()
 * @ORM\Table(name="user")
 */
class User
{
    /**
     * @ORM\id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue
     */
    protected $id;
    
    /**
     * @ORM\Column(name="email")
     */
    protected $email;
    
    /**
     * @ORM\Column(name="full_name")
     */
    protected $fullName;

    /**
     * @ORM\Column(name="emploi")
     */
    protected $emploi;
    
    /**
     * @ORM\Column(name="fonction")
     */
    private $fonction;
    
    /**
     * @ORM\Column(name="service")
     */
    private $service;
    
    /**
     * @ORM\Column(name="password")
     */
    private $password;
    
    /**
     * @ORM\Column(name="status")
     */
    private $status;
    
    /**
     * @ORM\Column(name="date_created")
     */
    private $dateCreated;
    
     /**
     * @ORM\Column(name="pwd_reset_token")
     */
    protected $pwdResetToken;
    
     /**
     * @ORM\Column(name="pwd_reset_token_creation_date")
     */
    protected $pwdResetTokenCreationDate;
    
    
    
    
    /**
     * Constructor.
     */
    public function __construct() 
    {
        //$this->collaborators = new ArrayCollection();
    }
    
    
    function getId() {
        return $this->id;
    }

    function getEmail() {
        return $this->email;
    }

    function getFullName() {
        return $this->fullName;
    }

    function getEmploi() {
        return $this->emploi;
    }

    function getFonction() {
        return $this->fonction;
    }

    function getService() {
        return $this->service;
    }

    function getPassword() {
        return $this->password;
    }

    function getStatus() {
        return $this->status;
    }
    
    function getDateCreated() {
        return $this->dateCreated;
    }
    
    function getPwdResetToken() {
        return $this->pwdResetToken;
    }
    
    function getPwdResetTokenCreationDate() {
        return $this->pwdResetTokenCreationDate;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setFullName($fullName) {
        $this->fullName = $fullName;
    }

    function setEmploi($emploi) {
        $this->emploi = $emploi;
    }

    function setFonction($fonction) {
        $this->fonction = $fonction;
    }

    function setService($service) {
        $this->service = $service;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setDateCreated($dateCreated) {
        $this->dateCreated = $dateCreated;
    }

    function setPwdResetToken($pwdResetToken) {
        $this->pwdResetToken = $pwdResetToken;
    }

    function setPwdResetTokenCreationDate($pwdResetTokenCreationDate) {
        $this->pwdResetTokenCreationDate = $pwdResetTokenCreationDate;
    }
    
    function toArray(){
        return get_object_vars($this);
    }

}



