<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a product.
 * @ORM\Entity()
 * @ORM\Table(name="validation")
 */
class Validation
{
    /**
     * @ORM\id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(name="pointId")
     */
    protected $pointId;
    
    /**
     * @ORM\Column(name="validationState")
     */
    private $validationState;
    
    /**
     * @ORM\Column(name="validationDate")
     */
    private $validationDate;
    
    
    function getId() {
        return $this->id;
    }

    function getPointId() {
        return $this->pointId;
    }

    function getValidationState() {
        return $this->validationState;
    }
    
    function getValidationDate() {
        return $this->validationDate;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPointId($pointId) {
        $this->pointId = $pointId;
    }

    function setValidationState($validationState) {
        $this->validationState = $validationState;
    }
    
    function setValidationDate($validationDate) {
        $this->validationDate = $validationDate;
    }

}



