<?php
namespace Application\Form\Point;

use Zend\Form\Form;

/**
 * This form is used to collect user's email, full name, password and status. The form 
 * can work in two scenarios - 'create' and 'update'. In 'create' scenario, user
 * enters password, in 'update' scenario he/she doesn't enter password.
 */
class PointForm extends Form
{
    /**
     * Scenario ('create' or 'update').
     * @var string 
     */
    private $scenario;
    
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    private $entityManager = null;
    
    /**
     * Current product.
     * @var Application\Entity\Point
     */
    private $point = null;
    
    /**
     * Constructor.     
     */
    public function __construct($scenario = 'create', $entityManager = null, $point = null)
    {
        // Define form name
        parent::__construct('point-form');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');
        
        // Save parameters for internal use.
        $this->scenario = $scenario;
        $this->entityManager = $entityManager;
        $this->point = $point;
        
        $this->addElements();
        $this->addInputFilter();
    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        if($this->scenario == 'update'){
            $this->add([            
                'type'  => 'hidden',
                'name' => 'returnUrl'
            ]);
        }
        
        $this->add([            
            'type'  => 'select',
            'name' => 'domaine',
            'options' => [
                'empty_option' => "::: Sélectionnez le projet :::",
                'label' => 'Domaine / Réunion / Projet',
            ],
        ]);
        
        $this->add([            
            'type'  => 'text',
            'name' => 'anotherDomaine',            
            'options' => [
                'label' => 'Autre domaine',
            ],
        ]);
        
        $this->add([            
            'type'  => 'textarea',
            'name' => 'activity',            
            'options' => [
                'label' => 'Activité',
            ],
        ]);
        
        $this->add([            
            'type'  => 'date',
            'name' => 'expiryDate',
            'options' => [
                'label' => 'Date d\'échéance',
            ],
        ]);
        
        $this->add([            
            'type'  => 'date',
            'name' => 'startDate',
            'options' => [
                'label' => 'Date de début',
            ],
        ]);

        $this->add([            
            'type'  => 'date',
            'name' => 'endDate',
            'options' => [
                'label' => 'Date de fin',
            ],
        ]);
        
        /*$this->add([            
            'type'  => 'select',
            'name' => 'collaborators',
            'attributes' => [
                'multiple' => 'multiple',
            ],
            'options' => [
                'empty_option' => '',
                'label' => 'Ajouter des collaborateurs sur cette activité',
            ],
        ]);*/
        
        $this->add([            
            'type'  => 'select',
            'name' => 'executionState',
            'options' => [
                'empty_option' => "::: Sélectionnez l'etat :::",
                'label' => 'Etat d\'exécution',
            ],
        ]);
        
        $this->add([            
            'type'  => 'number',
            'name' => 'purcent',
            'options' => [
                'label' => 'Pourcentage d\'exécution',
            ],
            'attributes' => [
                'min' => 0,
                'max' => 100
            ],
        ]);
        
        $this->add([            
            'type'  => 'textarea',
            'name' => 'observation',
            'options' => [
                'label' => 'Observations',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600
                ]
            ],
        ]);
        
        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'annuler',
            'attributes' => [                
                'value' => 'Retour'
            ],
        ]);
        
        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'enregistrer',
            'attributes' => [                
                'value' => 'Enregistrer'
            ],
        ]);
    }
    
    private function addInputFilter() 
    {
        // Create main input filter
        $inputFilter = $this->getInputFilter();        
                
        // Add input for "email" field
        $inputFilter->add([
                'name'     => 'observation',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],                    
                ],
                'validators' => [
                ],
            ]);
        
        $inputFilter->add([
                'name'     => 'activity',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],                    
                ],
                'validators' => [
                ],
            ]);
        
        $inputFilter->add([
                'name'     => 'purcent',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],                    
                ],
                'validators' => [
                ],
            ]);
        
        $inputFilter->add([
                'name'     => 'anotherDomaine',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],                    
                ],
                'validators' => [
                ],
            ]);
        $inputFilter->add([
                'name'     => 'expiryDate',
                'required' => true,
                'filters'  => [                    
                ],                
                'validators' => [
                ],
            ]);
        
        // Add input for "password" field
        $inputFilter->add([
                'name'     => 'endDate',
                'required' => false,
                'filters'  => [                    
                ],                
                'validators' => [
                ],
            ]); 
        
        // Add input for "password" field
        $inputFilter->add([
                'name'     => 'collaborators',
                'required' => false,
                'filters'  => [                    
                ],                
                'validators' => [
                ],
            ]); 
    }        
      
    public function isValid() {
        $result = parent::isValid();
        
        //var_dump($this->get('endDate')->getValue()); die;
        if($this->get('executionState')->getValue()==3 && $this->get('endDate')->getValue()==''){
            $this->get('endDate')->setMessages(['error'=> "Value is required and can't be empty"]);
            $result = false;
        }
        
        if($this->get('endDate')->getValue()!='' && $this->get('executionState')->getValue()!=3){
            $this->get('executionState')->setMessages(['error'=> "Vous avez indiqué une date de fin, l'activité est donc Terminé."]);
            $result = false;
        }
        
        if( ( !empty($this->get('startDate')->getValue())&& !empty($this->get('endDate')->getValue()) )  &&  $this->get('startDate')->getValue() > $this->get('endDate')->getValue()){
            $this->get('endDate')->setMessages(['error'=> "La date de fin doit être supérieure à celle de début."]);
            $result = false;
        }
        
        if($this->get('purcent')->getValue()<0 && $this->get('purcent')->getValue()>100){
            $this->get('purcent')->setMessages(['error'=> "Le pourcentage doit être compris entre 0% et 100%"]);
            $result = false;
        }
        
        if(($this->get('purcent')->getValue()<100 && $this->get('executionState')->getValue()==3) || 
                ($this->get('purcent')->getValue()==100 && $this->get('executionState')->getValue()!=3)){
            $this->get('purcent')->setValue(['error'=> "Veuillez corriger le pourcentage en fonction de l'état de l'activité."]);
            $result = false;
        }
        
        return $result;
    }
}