<?php
namespace Application\Form\SubDirection;

use Zend\Form\Form;

/**
 * This form is used to collect user's email, full name, password and status. The form 
 * can work in two scenarios - 'create' and 'update'. In 'create' scenario, user
 * enters password, in 'update' scenario he/she doesn't enter password.
 */
class DirectionForm extends Form
{
    /**
     * Scenario ('create' or 'update').
     * @var string 
     */
    private $scenario;
    
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    private $entityManager = null;
    
    /**
     * Current product.
     * @var Application\Entity\SubDirection
     */
    private $subDirection = null;
    
    /**
     * Constructor.     
     */
    public function __construct($scenario = 'create', $entityManager = null, $subDirection = null)
    {
        // Define form name
        parent::__construct('direction-form');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');
        
        // Save parameters for internal use.
        $this->scenario = $scenario;
        $this->entityManager = $entityManager;
        $this->subDirection = $subDirection;
        
        $this->addElements();          
    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        
        
        $this->add([            
            'type'  => 'text',
            'name' => 'subDirectionSigle',            
            'options' => [
                'label' => 'Sigle de la Sous-Direction',
            ],
        ]);
        
        $this->add([            
            'type'  => 'text',
            'name' => 'subDirectionName',            
            'options' => [
                'label' => 'Nom de la Sous-Direction',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600
                ]
            ],
        ]);
        
        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'annuler',
            'attributes' => [                
                'value' => 'Retour'
            ],
        ]);
        
        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'enregistrer',
            'attributes' => [                
                'value' => 'Enregistrer'
            ],
        ]);
    }
    
}