<?php
namespace Application\Form\Point;

use Zend\Form\Form;

/**
 * This form is used to collect user's email, full name, password and status. The form 
 * can work in two scenarios - 'create' and 'update'. In 'create' scenario, user
 * enters password, in 'update' scenario he/she doesn't enter password.
 */
class Recherche extends Form
{
    /**
     * Scenario ('create' or 'update').
     * @var string 
     */
    private $scenario;
    
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    private $entityManager = null;
    
    /**
     * Current user.
     * @var User\Entity\User 
     */
    private $user = null;
    
    /**
     * Constructor.     
     */
    public function __construct($scenario = 'create', $entityManager = null, $product = null)
    {
        // Define form name
        parent::__construct('recherche');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');
        
        // Save parameters for internal use.
        $this->scenario = $scenario;
        $this->entityManager = $entityManager;
        $this->product = $product;
        
        $this->addElements();          
    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
          
        $this->add([            
            'type'  => 'date',
            'name' => 'start',            
            'options' => [
                'label' => 'Entre le',
            ],
        ]);
        
        $this->add([            
            'type'  => 'date',
            'name' => 'end',            
            'options' => [
                'label' => 'Et le',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600
                ]
            ],
        ]);
        
        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'rechercher',
            'attributes' => [                
                'value' => 'Rechercher'
            ],
        ]);
        
    }
    
      
}