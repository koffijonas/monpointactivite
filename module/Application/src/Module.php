<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\MvcEvent;
use Zend\Session\SessionManager;
use Zend\EventManager\EventInterface;

class Module{
    const VERSION = '3.0.0dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
    
    /**
     * This method is called once the MVC bootstrapping is complete. 
     */
    function onBootstrap(EventInterface $e) {
        $application = $e->getApplication();
        $eventManager = $application->getEventManager();
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this,'onDispatchError'), 100); 

        $serviceManager = $application->getServiceManager();
        $sessionManager = $serviceManager->get(SessionManager::class);
        
        ////////////////////////////////////////////////////////////////
        $eventManager->attach(MvcEvent::EVENT_ROUTE, function($event){
            $routeMatch = $event->getRouteMatch(); 
            if ($routeMatch) {
                $event->getApplication()->getMvcEvent()->getViewModel()->setVariables([
                    'controller' => substr(explode("\\", $routeMatch->getParam('controller'))[2], 0, -10),
                    'action' => $routeMatch->getParam('action'),
                ]);
            }
        });
    }
 
    function onDispatchError(MvcEvent $e) {
        $viewModel = $e->getViewModel();
        $viewModel->setTerminal(true);
    }


    
}

