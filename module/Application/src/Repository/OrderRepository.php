<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\Delivery;
use Application\Entity\Supplier;
use Application\Entity\Order;

/**
 * This is the custom repository class for Order entity.
 */
class OrderRepository extends EntityRepository
{
    /**     
     * Retrieves all orders in descending dateCreated order.
     * @return Query
     */
    public function findAllOrders()
    {
        $entityManager = $this->getEntityManager();
        
        $queryBuilder = $entityManager->createQueryBuilder();
        
        $queryBuilder->select('o')
            ->from(Order::class, 'o')
            ->join(Supplier::class, 's', 'WITH', 's.id = o.supplierId')
            ->orderBy('o.date', 'DESC');
        
        return $queryBuilder->getQuery();
    }
    
    /**     
     * Retrieves orders THAT ARE NOT BEEN DELIVERED in descending dateCreated order.
     * @return Query
     */
    public function findNotDeliveredOrders()
    {
        $entityManager = $this->getEntityManager();
        
        $query = $entityManager->createQuery('SELECT o FROM Application\Entity\Order o WHERE not EXISTS ( SELECT d FROM Application\Entity\Delivery d join
            Application\Entity\Order b
            With b.id = d.orderId)');
        
        return $query;
    }
}