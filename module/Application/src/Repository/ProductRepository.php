<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\Product;
use Application\Entity\Category;

/**
 * This is the custom repository class for Product entity.
 */
class ProductRepository extends EntityRepository
{
    /**     
     * Retrieves all products in descending dateCreated order.
     * @return Query
     */
    public function findAllProducts()
    {
        $entityManager = $this->getEntityManager();
        
        $queryBuilder = $entityManager->createQueryBuilder();
        
        $queryBuilder->select('p')
            ->from(Product::class, 'p')
            ->join(Category::class, 'c', 'WITH', 'c.id = p.categoryId')
            ->orderBy('p.date', 'DESC');
        
        
        return $queryBuilder->getQuery();
    }
}