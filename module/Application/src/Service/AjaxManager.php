<?php
namespace Application\Service;

//use Application\Entity\SubDirection;
use Application\Entity\Point;
use Application\Entity\Project;
use Application\Entity\State;
use Application\Entity\Collaboration;
use Application\Entity\User;

/**
 * This service is responsible for adding/editing users
 * and changing user password.
 */
class AjaxManager
{
    
    /**
     * Auth service.
     * @var Zend\Authentication\Authentication
     */
    private $authService;
    
    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;  
    
    /**
     * Constructs the service.
     */
    public function __construct($authService, $entityManager) 
    {
        $this->authService = $authService;
        $this->entityManager = $entityManager;
    }
    
    public function getMesActivites($params){
            
        $query = $this->entityManager->createQueryBuilder();
        $query->select(['p.projectName','a.activity','a.startDate','a.endDate','s.stateName','a.observation','a.id', 'a.expiryDate', 'a.purcent'])
                ->from(Point::class, 'a')
                ->join(Project::class, 'p', 'WITH', 'a.domaine=p.id')
                ->join(State::class, 's', 'WITH', 's.id=a.executionState')
                ->join(User::class, 'u', 'WITH', 'u.email = :uid')
                ->join(Collaboration::class, 'c', 'WITH', 'c.userId=u.id AND c.pointId=a.id')
                //->leftJoin('(SELECT COUNT() as nbr FROM Application\Entity\Collaboration )')
                
                //->where('a.userId = :uid')
                ->setParameter('uid', $this->authService->getIdentity())
                ;
        
        if(empty($params['dateDebut']) && empty($params['dateFin']) && empty($params['executionState'])){
            $dernierLundi = date('Y-m-d', strtotime('monday'))==date('Y-m-d') ? date('Y-m-d', strtotime('monday')) : date('Y-m-d',strtotime('last monday'));
            $query->andWhere('(a.startDate >= :dd')->setParameter('dd', $dernierLundi);
            $query->orWhere('a.endDate >= :df')->setParameter('df', $dernierLundi);
            $query->orWhere('a.executionState=2)');
            
        }else if(!empty($params['dateDebut']) && !empty($params['dateFin'])){
            //$query->andWhere( $query->expr()->between('a.startDate', $params['dateDebut'], $params['dateFin']) );
            $query->andWhere('(a.startDate >= ?1')->setParameter(1, $params['dateDebut']);
            $query->andWhere('a.startDate <= ?2)')->setParameter(2, $params['dateFin']);
            
            //$query->orWhere( $query->expr()->between('a.endDate', $params['dateDebut'], $params['dateFin']) );
            $query->orWhere('(a.endDate >= ?3')->setParameter(3, $params['dateDebut']);
            $query->andWhere("(a.endDate!='0000-00-00' AND a.endDate <= ?4) )")->setParameter(4, $params['dateFin']);
            
        }else{
            if(isset($params['dateDebut']) && !empty($params['dateDebut'])){
                $query->andWhere('(a.startDate >= ?1')->setParameter(1, $params['dateDebut']);
                $query->orWhere('a.endDate >= ?2)')->setParameter(2, $params['dateDebut']);
            }

            if(isset($params['dateFin']) && !empty($params['dateFin'])){
                $query->andWhere('(a.startDate <= ?3')->setParameter(3, $params['dateFin']);
                $query->orWhere("(a.endDate!='0000-00-00' AND a.endDate <= ?4) )")->setParameter(4, $params['dateFin']);
            }
        }
        
        if(isset($params['executionState']) && !empty($params['executionState'])){
            $query->andWhere('a.executionState=:st')->setParameter('st', $params['executionState']);
        }
        
        if(isset($params['isExportExcel']) && !empty($params['isExportExcel'])){
            $query->orderBy('p.projectName', 'ASC');
        }
        
        //var_dump($query->getDql()); die;
        
        return $query;
    }
    
}

