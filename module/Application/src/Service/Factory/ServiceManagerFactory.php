<?php
namespace Application\Service\Factory;

use Interop\Container\ContainerInterface;
use Application\Service\ServiceManager;

/**
 * This is the factory class for RbacAssertionManager service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class ServiceManagerFactory
{
    /**
     * This method creates the NavManager service and returns its instance. 
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {   
        $authService = $container->get(\Zend\Authentication\AuthenticationService::class);
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        
        
        return new ServiceManager($authService, $entityManager);
    }
}
