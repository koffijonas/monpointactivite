<?php
namespace Application\Service;

/**
 * This service is responsible for determining which items should be in the main menu.
 * The items may be different depending on whether the user is authenticated or not.
 */
class NavManager
{
    /**
     * Auth service.
     * @var Zend\Authentication\Authentication
     */
    private $authService;
    
    /**
     * Url view helper.
     * @var Zend\View\Helper\Url
     */
    private $urlHelper;
    
    /**
     * RBAC manager.
     * @var User\Service\RbacManager
     */
    private $rbacManager;
    
    /**
     * Constructs the service.
     */
    public function __construct($authService, $urlHelper, $rbacManager) 
    {        //var_dump($urlHelper); die;
        $this->authService = $authService;
        $this->urlHelper = $urlHelper;
        $this->rbacManager = $rbacManager;
    }
    
    /**
     * This method returns menu items depending on whether user has logged in or not.
     */
    public function getMenuItems() 
    {
        $url = $this->urlHelper;
        $items = [];
        
        
        
        // Display "Login" menu item for not authorized user only. On the other hand,
        // display "Admin" and "Logout" menu items only for authorized users.
        if (!$this->authService->hasIdentity()) {
            /*$items[] = [
                'id' => 'login',
                'label' => 'Sign in',
                'icon' => 'notika-icon notika-house',
                'link'  => $url('login'),
                'float' => 'right'
            ];*/
        } else {
            $items[] = [
                'id' => 'home',
                'label' => '  Accueil',
                'icon' => 'fa fa-home',
                'link'  => $url('home'),

            ];
            
            $pointDropdownItems = array(
                [
                    'label' => "  Mes activités",
                    'icon' => 'fa fa-list-alt',
                    'link' => $url('pointactivites'),
                ],
                [
                    'label' => "  Ajouter une activité",
                    'icon' => 'fa fa-plus',
                    'link' => $url('addpointactivite'),
                ]
            );
            
            if ($this->rbacManager->isGranted(null, 'service.own.manage')) {
                $pointDropdownItems[] = [
                    'label' => "  Les activités de mes collaborateurs (agents)",
                    'icon' => 'fa fa-users',
                    'link' => $url('pointactivites', ['action'=>'pointsDeMesAgents']),
                ];
            }
            if ($this->rbacManager->isGranted(null, 'subDirectionPoints.own.manage')) {
                $pointDropdownItems[] = [
                    'label' => "  Les activités de mes services",
                    'icon' => 'fa fa-home',
                    'link' => $url('pointactivites', ['action'=>'pointsDeMesServices']),
                ];
            }
            $items[] = [
                'id' => 'point',
                'label' => '  Point d\'activités',
                'icon' => 'fa fa-tasks',
                'link'  => $url('pointactivites'),
                'dropdown' => $pointDropdownItems
            ];
            
            $paramDropdownItems = [];
            if ($this->rbacManager->isGranted(null, 'subDirection.manage')) {
                $paramDropdownItems[] = [
                            'label' => "  Sous-Directions",
                            'icon' => 'fa fa-home',
                            'link' => $url('subDirections'),
                        ];
            }
            if ($this->rbacManager->isGranted(null, 'service.manage')) {
                $paramDropdownItems[] = [
                            'label' => '  Services',
                            'icon' => 'fa fa-home',
                            'link' => $url('services'),
                        ];
                $paramDropdownItems[] = [
                            'label' => '  Statut d\'activité',
                            'icon' => 'fa fa-line-chart',
                            'link' => $url('states'),
                        ];
            }
            if ($this->rbacManager->isGranted(null, 'serviceProjects.manage')) {
                $paramDropdownItems[] = [
                            'label' => '  Projets',
                            'icon' => 'fa fa-list',
                            'link' => $url('projects'),
                        ];
            }
             if (count($paramDropdownItems)!=0){
                 $items[] = [
                    'id' => 'parametre',
                    'label' => '  Paramètres',
                    'float' => 'right',
                    'link' => '#',
                    'icon' => 'fa fa-gears',
                    'dropdown' => $paramDropdownItems
                ];
             }
            
            $items[] = [
                'id' => 'compte',
                'label' => '  Mon compte',
                'icon' => 'fa fa-user',
                'link'  => $url('users', ['action'=>'view']),

            ];
            
            
            // Determine which items must be displayed in Admin dropdown.
            $adminDropdownItems = [];
            
            if ($this->rbacManager->isGranted(null, 'user.manage') || $this->rbacManager->isGranted(null, 'service.own.manage')) {
                $adminDropdownItems[] = [
                            'id' => 'users',
                            'label' => '  Gestion des utilisateurs',
                            'icon' => 'fa fa-users',
                            'link' => $url('users')
                        ];
            }
            
            if ($this->rbacManager->isGranted(null, 'permission.manage')) {
                $adminDropdownItems[] = [
                            'id' => 'permissions',
                            'label' => '  Gestion des permissions',
                            'icon' => 'fa fa-ban',
                            'link' => $url('permissions')
                        ];
            }
            
            if ($this->rbacManager->isGranted(null, 'role.manage')) {
                $adminDropdownItems[] = [
                            'id' => 'roles',
                            'label' => '  Gestion des rôles',
                            'icon' => 'fa fa-ban',
                            'link' => $url('roles')
                        ];
            }
            
            if (count($adminDropdownItems)!=0) {
                $items[] = [
                    'id' => 'admin',
                    'label' => '  Gestion des comptes',
                    'icon' => 'fa fa-users',
                    'link' => 'admin',
                    'dropdown' => $adminDropdownItems
                ];
            }
            
            /*$items[] = [
                'id' => 'logout',
                'label' => $this->authService->getIdentity(),
                'float' => 'right',
                'link' => 'logout',
                'icon' => 'notika-icon notika-house',
                'dropdown' => [
                    [
                        'id' => 'settings',
                        'label' => 'Settings',
                        'icon' => 'notika-icon notika-house',
                        'link' => $url('application', ['action'=>'settings'])
                    ],
                    [
                        'id' => 'logout',
                        'label' => 'Sign out',
                        'icon' => 'notika-icon notika-house',
                        'link' => $url('logout')
                    ],
                ]
            ];*/
        }
        
        return $items;
    }
}


