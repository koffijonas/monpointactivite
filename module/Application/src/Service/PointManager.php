<?php
namespace Application\Service;

use Application\Entity\Point;
use Application\Entity\Project;
use Application\Entity\State;
use Application\Entity\Service;
use Application\Entity\SubDirection;
use User\Entity\User;
use Application\Entity\Collaboration;
use Application\Entity\Validation;

/**
 * This service is responsible for adding/editing users
 * and changing user password.
 */
class PointManager
{
    
    /**
     * Auth service.
     * @var Zend\Authentication\Authentication
     */
    private $authService;
    
    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;  
    
    /**
     * Constructs the service.
     */
    public function __construct($authService, $entityManager) 
    {
        $this->authService = $authService;
        $this->entityManager = $entityManager;
    }
    
   
    /**
     * This method adds a new point.
     */
    public function addPoint($data, $userConnected) 
    {
        // Create new Product entity.
        $point = new Point();
        $currentDate = date('Y-m-d H:i:s');
        $point->setAddDate($currentDate);
        $point->setDomaine($data['domaine']);
        $point->setActivity($data['activity']);
        $point->setExpiryDate($data['expiryDate']);
        $point->setStartDate($data['startDate']);
        $point->setEndDate($data['endDate']);
        $point->setExecutionState($data['executionState']);
        $point->setPurcent($data['purcent']);
        $point->setObservation($data['observation']);
        $point->addCollaborator($userConnected);
        $point->setUserId($userConnected->getId());           
        
        // Add the entity to the entity manager.
        $this->entityManager->persist($point);
                       
        // Apply changes to database.
        $this->entityManager->flush();
        
        return $point;
    }
    
    public function validatePoints($points) 
    {
        // Create new Product entity.
        foreach ($points as $point){
            $validation = new Validation();
            
            $validation->setPointId($point['id']);
            $validation->setValidationState(1);
            $currentDate = date('Y-m-d H:i:s');
            $validation->setValidationDate($currentDate);          

            // Add the entity to the entity manager.
            $this->entityManager->persist($validation);

            // Apply changes to database.
            $this->entityManager->flush();
        }
        
        return ;
    }
    
     public function validatePointAgents($params){
         
        $allPoints = $this->getActivitesDesAgents($params);
        //supprimer les points dupliqués dû à la collaboration
        $allPointsWithoutDuplicate = array(); // le nouveau tableau dédoublonné
        $pointIds = array(); // contiendra les ids à éviter
        foreach($allPoints as $point)
        {
          if(!in_array($point['id'], $pointIds)) {
             $allPointsWithoutDuplicate[] = $point;
             $pointIds[] = $point['id'];   
          }
        }
        
        $this->validatePoints($allPointsWithoutDuplicate);
            
        return ;
    }
    
    /**
     * This method updates data of an existing user.
     */
    public function updatePoint($point, $data) 
    {
        
        $point->setDomaine($data['domaine']);
        $point->setActivity($data['activity']);
        $point->setExpiryDate($data['expiryDate']);
        $point->setStartDate($data['startDate']);
        $point->setEndDate($data['endDate']);
        $point->setExecutionState($data['executionState']);
        $point->setPurcent($data['purcent']);
        $point->setObservation($data['observation']); 
        // Apply changes to database.
        $this->entityManager->flush();

        return true;
    }
    
     /**
     * delete the given point.
     */
    public function assignCollaborators($point, $collaboratorsIds, $userConnected)
    {
        // Remove old user role(s).
        $point->getCollaborators()->clear();
        
        // Assign new role(s).
        foreach ($collaboratorsIds as $collaboratorId) {
            $collaborator = $this->entityManager->getRepository(User::class)
                    ->find($collaboratorId);
            if ($collaborator==null) {
                throw new \Exception('Aucun Utilisateur ne correspond à  cet ID');
            }
            //var_dump($collaborator); die;
            $point->addCollaborator($collaborator);
        }
        
        $point->addCollaborator($userConnected);
    }
    
    /**
     * delete the given point.
     */
    public function deletePoint($point)
    {
        $this->entityManager->remove($point);
        $this->entityManager->flush();
        
    }
    
    public function getActivitesDesAgents($params){
        $query = $this->entityManager->createQueryBuilder();
        
        $query->select(['p.projectName','a.activity', 'a.expiryDate','a.startDate','a.endDate','s.stateName','a.observation','a.id','u.email', 'a.purcent'])
                ->from(Point::class, 'a')
                ->join(Project::class, 'p', 'WITH', 'a.domaine=p.id')
                ->join(State::class, 's', 'WITH', 's.id=a.executionState')
                ->join(User::class, 'u', 'WITH', 'u.service = :uid')
                ->join(Collaboration::class, 'c', 'WITH', 'c.userId=u.id AND c.pointId=a.id')
                
                //->where('a.userId = :uid')
                ->setParameter('uid', $params['user']->getService())
                ;
        
        if(empty($params['dateDebut']) && empty($params['dateFin']) && empty($params['executionState'])){
            $dernierLundi = date('Y-m-d', strtotime('monday'))==date('Y-m-d') ? date('Y-m-d', strtotime('monday')) : date('Y-m-d',strtotime('last monday'));
            $query->andWhere('(a.startDate >= :dd')->setParameter('dd', $dernierLundi);
            $query->orWhere('a.endDate >= :df')->setParameter('df', $dernierLundi);
            $query->orWhere('a.executionState=2)');
            
        }else if(!empty($params['dateDebut']) && !empty($params['dateFin'])){
            //$query->andWhere( $query->expr()->between('a.startDate', $params['dateDebut'], $params['dateFin']) );
            $query->andWhere('(a.startDate >= ?1')->setParameter(1, $params['dateDebut']);
            $query->andWhere('a.startDate <= ?2)')->setParameter(2, $params['dateFin']);
            
            //$query->orWhere( $query->expr()->between('a.endDate', $params['dateDebut'], $params['dateFin']) );
            $query->orWhere('(a.endDate >= ?3')->setParameter(3, $params['dateDebut']);
            $query->andWhere("(a.endDate!='0000-00-00' AND a.endDate <= ?4) )")->setParameter(4, $params['dateFin']);
            
        }else{
            if(isset($params['dateDebut']) && !empty($params['dateDebut'])){
                $query->andWhere('(a.startDate >= ?1')->setParameter(1, $params['dateDebut']);
                $query->orWhere('a.endDate >= ?2)')->setParameter(2, $params['dateDebut']);
            }

            if(isset($params['dateFin']) && !empty($params['dateFin'])){
                $query->andWhere('(a.startDate <= ?3')->setParameter(3, $params['dateFin']);
                $query->orWhere("(a.endDate!='0000-00-00' AND a.endDate <= ?4) )")->setParameter(4, $params['dateFin']);
            }
        }
        
        if(isset($params['agent']) && !empty($params['agent'])){
            $query->andWhere('u.id = :ag')->setParameter('ag', $params['agent']);
        }
        
        if(isset($params['executionState']) && !empty($params['executionState'])){
            $query->andWhere('a.executionState=:st')->setParameter('st', $params['executionState']);
        }
        
        if(isset($params['isExportExcel']) && !empty($params['isExportExcel'])){
            $query->orderBy('p.projectName', 'ASC');
        }
        
        return $query->getQuery()->getResult();
    }

    public function getActivitesDesServices($params){
        $query = $this->entityManager->createQueryBuilder();
        
        $query->select(['p.projectName','a.activity', 'a.expiryDate', 'a.startDate','a.endDate','s.stateName','a.observation','a.id','u.service','u.fullName'])
                ->from(Point::class, 'a')
                ->join(Project::class, 'p', 'WITH', 'a.domaine=p.id')
                ->join(State::class, 's', 'WITH', 's.id=a.executionState')
                ->join(SubDirection::class, 'd', 'WITH', 'd.id = :uid')
                ->join(Service::class, 'o', 'WITH', 'o.subDirectionId = d.id')
                ->join(User::class, 'u', 'WITH', 'u.service = o.id')
                ->join(Collaboration::class, 'c', 'WITH', 'c.userId=u.id AND c.pointId=a.id')
                
                //->where('a.userId = :uid')
                ->setParameter('uid', $params['user']->getService())
                ;
        
        if(empty($params['dateDebut']) && empty($params['dateFin']) && empty($params['executionState'])){
            $dernierLundi = date('Y-m-d', strtotime('monday'))==date('Y-m-d') ? date('Y-m-d', strtotime('monday')) : date('Y-m-d',strtotime('last monday'));
            $query->andWhere('(a.startDate >= :dd')->setParameter('dd', $dernierLundi);
            $query->orWhere('a.endDate >= :df')->setParameter('df', $dernierLundi);
            $query->orWhere('a.executionState=2)');
            
        }else if(!empty($params['dateDebut']) && !empty($params['dateFin'])){
            //$query->andWhere( $query->expr()->between('a.startDate', $params['dateDebut'], $params['dateFin']) );
            $query->andWhere('(a.startDate >= ?1')->setParameter(1, $params['dateDebut']);
            $query->andWhere('a.startDate <= ?2)')->setParameter(2, $params['dateFin']);
            
            //$query->orWhere( $query->expr()->between('a.endDate', $params['dateDebut'], $params['dateFin']) );
            $query->orWhere('(a.endDate >= ?3')->setParameter(3, $params['dateDebut']);
            $query->andWhere("(a.endDate!='0000-00-00' AND a.endDate <= ?4) )")->setParameter(4, $params['dateFin']);
            
        }else{
            if(isset($params['dateDebut']) && !empty($params['dateDebut'])){
                $query->andWhere('(a.startDate >= ?1')->setParameter(1, $params['dateDebut']);
                $query->orWhere('a.endDate >= ?2)')->setParameter(2, $params['dateDebut']);
            }

            if(isset($params['dateFin']) && !empty($params['dateFin'])){
                $query->andWhere('(a.startDate <= ?3')->setParameter(3, $params['dateFin']);
                $query->orWhere("(a.endDate!='0000-00-00' AND a.endDate <= ?4) )")->setParameter(4, $params['dateFin']);
            }
        }
        
        if(isset($params['service']) && !empty($params['service'])){
            $query->andWhere('u.service = :ser')->setParameter('ser', $params['service']);
        }
        
        if(isset($params['executionState']) && !empty($params['executionState'])){
            $query->andWhere('a.executionState=:st')->setParameter('st', $params['executionState']);
        }
        
        if(isset($params['isExportExcel']) && !empty($params['isExportExcel'])){
            $query->orderBy('p.projectName', 'ASC');
        }
        
        return $query->getQuery()->getResult();
    }
    
    public function getNombreProjetByUser($userConnected){
        $query = $this->entityManager->createQueryBuilder();
        
        if($userConnected->getFunction()=='Sous-Directeur'){
            $query->select($query->expr()->countDistinct('a.domaine'))
                    ->from(Point::class, 'a')
                    ->join(Collaboration::class, 'c', 'WITH', 'c.pointId=a.id')
                    ->join(Service::class, 's', 'WITH', 's.subDirectionId=?1')
                    ->join(User::class, 'u', 'WITH', 'u.id = c.userId AND u.service=s.id')
                    //->where('u.fonction != ?2')
                    //->andWhere('')
                    ->setParameter(1, $userConnected->getService())
                    //->setParameter(2, 'Sous-Directeur')
                    ;
            
        }else if($userConnected->getFunction()=='Chef de service'){
            $query->select($query->expr()->countDistinct('a.domaine'))
                    ->from(Point::class, 'a')
                    ->join(Collaboration::class, 'c', 'WITH', 'c.pointId=a.id')
                    ->join(User::class, 'u', 'WITH', 'u.id = c.userId AND u.service=?1')
                    //->where('u.email = ?1')
                    //->andWhere('')
                    ->setParameter(1, $userConnected->getService())
                    ;
            
        }else{
            $query->select($query->expr()->countDistinct('a.domaine'))
                    ->from(Point::class, 'a')
                    ->join(Collaboration::class, 'c', 'WITH', 'c.pointId=a.id')
                    ->join(User::class, 'u', 'WITH', 'u.id = c.userId')
                    ->where('u.email = ?1')
                    //->andWhere('')
                    ->setParameter(1, $this->authService->getIdentity())
                    ;
        }
        
        //var_dump($query->getQuery()->getResult()); die;
        //return count($query->getQuery()->getResult());
        return $query->getQuery()->getResult()[0][1];
    }
    
    public function getNbrActiviteEndByUser($userConnected){
        $query = $this->entityManager->createQueryBuilder();
        
        if($userConnected->getFunction()=='Sous-Directeur'){
            $query->select($query->expr()->countDistinct('a.id'))
                ->from(Point::class, 'a')
                ->join(Collaboration::class, 'c', 'WITH', 'c.pointId=a.id')
                ->join(Service::class, 's', 'WITH', 's.subDirectionId=?1')
                ->join(User::class, 'u', 'WITH', 'u.id = c.userId AND u.service=s.id')
                //->where('u.email = ?1')
                ->andWhere('a.executionState=3')
                //->setParameter(1, $this->authService->getIdentity())
                ->setParameter(1, $userConnected->getService())
                ;
            
        }else if($userConnected->getFunction()=='Chef de service'){
            $query->select($query->expr()->countDistinct('a.id'))
                ->from(Point::class, 'a')
                ->join(Collaboration::class, 'c', 'WITH', 'c.pointId=a.id')
                ->join(User::class, 'u', 'WITH', 'u.id = c.userId AND u.service=?1')
                //->where('u.email = ?1')
                ->andWhere('a.executionState=3')
                //->setParameter(1, $this->authService->getIdentity())
                ->setParameter(1, $userConnected->getService())
                ;
            
        }else{
            $query->select($query->expr()->countDistinct('a.id'))
                ->from(Point::class, 'a')
                ->join(Collaboration::class, 'c', 'WITH', 'c.pointId=a.id')
                ->join(User::class, 'u', 'WITH', 'u.id = c.userId')
                ->where('u.email = ?1')
                ->andWhere('a.executionState=3')
                ->setParameter(1, $this->authService->getIdentity())
                ;
        }
        
        //return count($query->getQuery()->getResult());
        return $query->getQuery()->getResult()[0][1];
    }
    
    public function getNbrActiviteNotEndByUser($userConnected){
        $query = $this->entityManager->createQueryBuilder();
        
        if($userConnected->getFunction()=='Sous-Directeur'){
            $query->select($query->expr()->countDistinct('a.id'))
                    ->from(Point::class, 'a')
                    ->join(Collaboration::class, 'c', 'WITH', 'c.pointId=a.id')
                    ->join(Service::class, 's', 'WITH', 's.subDirectionId=?1')
                    ->join(User::class, 'u', 'WITH', 'u.id = c.userId AND u.service=s.id')
                    //->where('u.email = ?1')
                    ->where('a.executionState != 3')
                    ->setParameter(1, $userConnected->getService())
                    ;
            
        }else if($userConnected->getFunction()=='Chef de service'){
            $query->select($query->expr()->countDistinct('a.id'))
                    ->from(Point::class, 'a')
                    ->join(Collaboration::class, 'c', 'WITH', 'c.pointId=a.id')
                    ->join(User::class, 'u', 'WITH', 'u.id = c.userId AND u.service=?1')
                    //->where('u.email = ?1')
                    ->where('a.executionState != 3')
                    ->setParameter(1, $userConnected->getService())
                    ;
            
        }else{
            $query->select($query->expr()->countDistinct('a.id'))
                    ->from(Point::class, 'a')
                    ->join(Collaboration::class, 'c', 'WITH', 'c.pointId=a.id')
                    ->join(User::class, 'u', 'WITH', 'u.id = c.userId')
                    ->where('u.email = ?1')
                    ->andWhere('a.executionState != 3')
                    ->setParameter(1, $this->authService->getIdentity())
                    ;
        }
        
        //return count($query->getQuery()->getResult());
        return $query->getQuery()->getResult()[0][1];
    }
    
    public function getNbrCollaborateurs($userConnected){
        
        //$users = $this->entityManager->getRepository(User::class)->findBy(['service'=>$userConnected->getService()], ['dateCreated'=>'DESC']);
        //$serviceUsersTotal = count($users);
        
        if($userConnected->getFunction()=='Sous-Directeur'){
            $query = $this->entityManager->createQueryBuilder();
            $query->select('u')
                    ->from(User::class, 'u')
                    ->join(Service::class, 's', 'WITH', 's.id=u.service')
                    ->where('u.function != ?1')
                    ->andWhere('s.subDirectionId = ?2')
                    ->setParameter(1, 'Sous-Directeur')
                    ->setParameter(2, $userConnected->getService())
                    ;
            return count($query->getQuery()->getResult());
            
        }else{
            $users = $this->entityManager->getRepository(User::class)->findBy(['service'=>$userConnected->getService()], ['dateCreated'=>'DESC']);
            return count($users)-1;
        }
        
        
        //return $query->getQuery()->getResult()[0][1];
    }
}

