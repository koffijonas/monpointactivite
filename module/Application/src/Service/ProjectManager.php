<?php
namespace Application\Service;

use Application\Entity\Project;

/**
 * This service is responsible for adding/editing users
 * and changing user password.
 */
class ProjectManager
{
    
    /**
     * Auth service.
     * @var Zend\Authentication\Authentication
     */
    private $authService;
    
    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;  
    
    /**
     * Constructs the service.
     */
    public function __construct($authService, $entityManager) 
    {
        $this->authService = $authService;
        $this->entityManager = $entityManager;
    }
    
    /**
     * This method adds a new project.
     */
    public function addProject($data) 
    {
        
        // Create new Product entity.
        $project = new Project();
        $currentDate = date('Y-m-d H:i:s');
        $project->setProjectName($data['projectName']);
        $project->setServiceId($data['service']);
        $project->setAddDate($currentDate);
        $project->setUserId($this->authService->getIdentity());           
        
        // Add the entity to the entity manager.
        $this->entityManager->persist($project);
                       
        // Apply changes to database.
        $this->entityManager->flush();
        
        return $project;
    }
    
    /**
     * This method updates data of an existing user.
     */
    public function updateProject($project, $data) 
    {
        
        $project->setProjectName($data['projectName']);
        $project->setServiceId($data['service']);
        
        // Apply changes to database.
        $this->entityManager->flush();

        return true;
    }
    
    /**
     * delete the given project.
     */
    public function deleteProject($project)
    {
        $this->entityManager->remove($project);
        $this->entityManager->flush();
        
    }
    
}

