<?php
namespace Application\Service;

use Application\Entity\Service;

/**
 * This service is responsible for adding/editing users
 * and changing user password.
 */
class ServiceManager
{
    
    /**
     * Auth service.
     * @var Zend\Authentication\Authentication
     */
    private $authService;
    
    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;  
    
    /**
     * Constructs the service.
     */
    public function __construct($authService, $entityManager) 
    {
        $this->authService = $authService;
        $this->entityManager = $entityManager;
    }
    
    /**
     * This method adds a new service.
     */
    public function addService($data) 
    {
        
        // Create new Product entity.
        $service = new Service();
        $currentDate = date('Y-m-d H:i:s');
        $service->setServiceName($data['serviceName']);
        $service->setSubDirectionId($data['subDirection']);
        $service->setAddDate($currentDate);
        $service->setUserId($this->authService->getIdentity());           
        
        // Add the entity to the entity manager.
        $this->entityManager->persist($service);
                       
        // Apply changes to database.
        $this->entityManager->flush();
        
        return $service;
    }
    
    /**
     * This method updates data of an existing user.
     */
    public function updateService($service, $data) 
    {
        
        $service->setServiceName($data['serviceName']);
        $service->setSubDirectionId($data['subDirection']);
        
        // Apply changes to database.
        $this->entityManager->flush();

        return true;
    }
    
    /**
     * delete the given service.
     */
    public function deleteService($service)
    {
        $this->entityManager->remove($service);
        $this->entityManager->flush();
        
    }
    
}

