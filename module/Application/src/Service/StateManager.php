<?php
namespace Application\Service;

use Application\Entity\State;

/**
 * This state is responsible for adding/editing users
 * and changing user password.
 */
class StateManager
{
    
    /**
     * Auth state.
     * @var Zend\Authentication\Authentication
     */
    private $authService;
    
    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;  
    
    /**
     * Constructs the state.
     */
    public function __construct($authService, $entityManager) 
    {
        $this->authService = $authService;
        $this->entityManager = $entityManager;
    }
    
    /**
     * This method adds a new state.
     */
    public function addState($data) 
    {
        
        // Create new Product entity.
        $state = new State();
        $currentDate = date('Y-m-d H:i:s');
        $state->setStateName($data['stateName']);
        $state->setAddDate($currentDate);
        $state->setUserId($this->authService->getIdentity());           
        
        // Add the entity to the entity manager.
        $this->entityManager->persist($state);
                       
        // Apply changes to database.
        $this->entityManager->flush();
        
        return $state;
    }
    
    /**
     * This method updates data of an existing user.
     */
    public function updateState($state, $data) 
    {
        
        $state->setStateName($data['stateName']);
        
        // Apply changes to database.
        $this->entityManager->flush();

        return true;
    }
    
    /**
     * delete the given state.
     */
    public function deleteState($state)
    {
        $this->entityManager->remove($state);
        $this->entityManager->flush();
        
    }
    
}

