<?php
namespace Application\Service;

use Application\Entity\SubDirection;

/**
 * This service is responsible for adding/editing users
 * and changing user password.
 */
class SubDirectionManager
{
    
    /**
     * Auth service.
     * @var Zend\Authentication\Authentication
     */
    private $authService;
    
    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;  
    
    /**
     * Constructs the service.
     */
    public function __construct($authService, $entityManager) 
    {
        $this->authService = $authService;
        $this->entityManager = $entityManager;
    }
    
    /**
     * This method adds a new subDirection.
     */
    public function addSubDirection($data) 
    {
        
        // Create new Product entity.
        $subDirection = new SubDirection();
        $currentDate = date('Y-m-d H:i:s');
        $subDirection->setSigleSousDirection($data['subDirectionSigle']);
        $subDirection->setLibSousDirection($data['subDirectionName']);
        $subDirection->setAddDate($currentDate);
        $subDirection->setUserId($this->authService->getIdentity());           
        
        // Add the entity to the entity manager.
        $this->entityManager->persist($subDirection);
                       
        // Apply changes to database.
        $this->entityManager->flush();
        
        return $subDirection;
    }
    
    /**
     * This method updates data of an existing user.
     */
    public function updateSubDirection($subDirection, $data) 
    {
        
        $subDirection->setSigleSousDirection($data['subDirectionSigle']);
        $subDirection->setLibSousDirection($data['subDirectionName']);
        
        // Apply changes to database.
        $this->entityManager->flush();

        return true;
    }
    
    /**
     * delete the given subDirection.
     */
    public function deleteSubDirection($subDirection)
    {
        $this->entityManager->remove($subDirection);
        $this->entityManager->flush();
        
    }
    
}

