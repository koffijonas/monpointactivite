<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use User\Entity\User;
use Application\Entity\Service;

/**
 * This view helper class displays a menu bar.
 */
class Menu extends AbstractHelper 
{
     /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    private $entityManager;
    
    /**
     * Authentication service.
     * @var Zend\Authentication\AuthenticationService 
     */
    private $authService;
    
    /**
     * Logged in user.
     * @var User\Entity\User
     */
    private $user = null;
    /**
     * Menu items array.
     * @var array 
    */
    protected $items = [];
    
    /**
     * Active item's ID.
     * @var string  
     */
    protected $activeItemId = '';
    
    /**
     * Constructor.
     * @param array $items Menu items.
     */
    public function __construct($entityManager, $authService, $items=[]) 
    {
        $this->items = $items;
        $this->entityManager = $entityManager;
        $this->authService = $authService;
        
    }
    
    /**
     * Sets menu items.
     * @param array $items Menu items.
     */
    public function setItems($items) 
    {
        $this->items = $items;
    }
    
    /**
     * Sets ID of the active items.
     * @param string $activeItemId
     */
    public function setActiveItemId($activeItemId) 
    {
        $this->activeItemId = $activeItemId;
    }
    
    public function getConnectedUser($useCachedUser = true){
        // If current user is already fetched, return it.
        if ($useCachedUser && $this->user!==null)
            return $this->user;
        
        // Check if user is logged in.
        if ($this->authService->hasIdentity()) {
            
            // Fetch User entity from database.
            $this->user = $this->entityManager->getRepository(User::class)
                    ->findOneByEmail($this->authService->getIdentity());
            if ($this->user==null) {
                // Oops.. the identity presents in session, but there is no such user in database.
                // We throw an exception, because this is a possible security problem. 
                throw new \Exception('Not found user with such email');
            }
            
            // Return found User.
            return $this->user;
        }
    }
    public function getUserServiceName($serviceId){
       
        $service = $this->entityManager->getRepository(Service::class)
                ->find($serviceId);
           
        return $service->getServiceName();
    }
    
    /**
     * Renders the main menu.
     * @return string HTML code of the menu.
    */
    public function render() 
    {
        if (count($this->items)==0)
            return ''; // Do nothing if there are no items.
        
       
        $result= '<ul class="nav navbar-nav">';
        // Render items
        foreach ($this->items as $item) {
            $result .= $this->renderItem($item);
        }
        
        $result .= '</ul>';
        
        return $result;   
    }
    
    /**
     * Renders an item.
     * @param array $item The menu item info.
     * @return string HTML code of the item.
     */
    protected function renderItem($item) 
    {
        $id = isset($item['id']) ? $item['id'] : '';
        $isActive = ($id==$this->activeItemId);
        $label = isset($item['label']) ? $item['label'] : '';
        $icon = $item['icon'];
             
        $result = ''; 
     
        $escapeHtml = $this->getView()->plugin('escapeHtml');
        
               
        $link = isset($item['link']) ? $item['link'] : '#';
         
        if (isset($item['dropdown'])) {
            
            $dropdownItems = $item['dropdown'];
            
            $result .= $isActive?'<li class="active dropdown">':'<li class="dropdown">';
            $result .= '<a href="#"><i class="' . $escapeHtml($icon) . '"></i>' . $escapeHtml($label) . ' <i class="pourmenu fa fa-caret-down" aria-hidden="true"></i></a>';
        
            $result .= '<ul>';
            foreach ($dropdownItems as $item) {
                $link = isset($item['link']) ? $item['link'] : '#';
                $label = isset($item['label']) ? $item['label'] : '';
                $icon = $item['icon'];
                
                $result .= $isActive?'<li class="active">':'<li>';
                $result .= '<a href="'.$escapeHtml($link).'"><span class="'.$escapeHtml($icon).'"></span>'.$escapeHtml($label).'</a>';
                $result .= '</li>';
            }
            $result .= '</ul>';
            
        } else {
            $result .= $isActive?'<li class="active">':'<li>';
            $result .= '<a href="' . $escapeHtml($link) . '"><i class="' . $escapeHtml($icon) . '"></i>' . $escapeHtml($label) . '</a>';
        }
        $result .= '</li>';
        
    
        return $result;
    }
    
}
