<?php
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Entity\User;
use User\Entity\Role;
use Application\Entity\Service;
use Application\Entity\SubDirection;
use User\Form\UserForm;
use User\Form\PasswordChangeForm;
use User\Form\PasswordResetForm;

/**
 * This controller is responsible for user management (adding, editing, 
 * viewing users and changing user's password).
 */
class UserController extends AbstractActionController 
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    
    /**
     * User manager.
     * @var User\Service\UserManager 
     */
    private $userManager;
    
    /**
     * User manager.
     */
    private $adminCheck;
    
    /**
     * Constructor. 
     */
    public function __construct($entityManager, $userManager)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
    }
    
    /**
     * This is the default "index" action of the controller. It displays the 
     * list of users.
     */
    public function indexAction() 
    {
        $allServices = $this->entityManager->getRepository(Service::class)
                ->findBy([], ['serviceName'=>'ASC']);
        $serviceList = [];
        foreach ($allServices as $service) {
            $serviceList[$service->getId()] = $service->getServiceName();
        }
        
        $allSubDirections = $this->entityManager->getRepository(SubDirection::class)
                ->findBy([], ['libSousDirection'=>'ASC']);
        $subDirectionList = [];
        foreach ($allSubDirections as $subDirection) {
            $subDirectionList[$subDirection->getId()] = $subDirection->getLibSousDirection();
        }
        
        $user = $this->currentUser();
        $userRoles = $user->getRoles();
        $users = [];
        foreach ($userRoles as $userRole){
            if($userRole->getId() == 1){
                $users = $this->entityManager->getRepository(User::class)
                    ->findBy([], ['dateCreated'=>'DESC']);
            }
            elseif($userRole->getId() == 5){
                $users = $this->entityManager->getRepository(User::class)
                    ->findBy(['service'=>$user->getService()], ['dateCreated'=>'DESC']);
                //retired users list
                $retiredUsers = $this->entityManager->getRepository(User::class)
                    ->findBy(['status'=>2], ['dateCreated'=>'DESC']);
                //add retired users tot the list
                foreach ($retiredUsers as $retiredUser){
                    if($retiredUser->getService() == $user->getService()){
                      $users[] = $user;   
                    }
                   
                }
            }
            else{
                $queryBuilder = $this->entityManager->createQueryBuilder();
        
                $queryBuilder->select('u')
                    ->from(User::class, 'u')
                    ->join(SubDirection::class, 'sd', 'WITH', 'sd.id = :sid')
                    ->join(Service::class, 's', 'WITH', 'sd.id = s.subDirectionId')
                    ->where("u.function = 'Chef de service'")
                    ->orderBy('u.dateCreated', 'DESC')
                    ->setParameter('sid', $user->getService());
                
                $users = $queryBuilder->getQuery()->getResult();
                
            }
        }
        	         
        return new ViewModel([
            'users' => $users,
            'services' => $serviceList,
            'subDirections' => $subDirectionList
        ]);
    } 
    
    /**
     * This action displays a page allowing to add a new user.
     */
    public function addAction()
    {
        $user = $this->currentUser();
        // Create user form
        $form = new UserForm('create', $this->entityManager, null, $user);
        
        $userRoles = $user->getRoles();
        foreach ($userRoles as $userRole){
            if($userRole->getId() == 1){
                
                $allServices = $this->entityManager->getRepository(Service::class)
                        ->findBy([], ['serviceName'=>'ASC']);
                $serviceList = [];
                foreach ($allServices as $service) {
                    $serviceList[$service->getId()] = $service->getServiceName();
                }
                $form->get('service')->setValueOptions($serviceList);
                
                $allRoles = $this->entityManager->getRepository(Role::class)
                        ->findBy([], ['name'=>'ASC']);
                $roleList = [];
                foreach ($allRoles as $role) {
                    $roleList[$role->getId()] = $role->getName();
                }
                $form->get('roles')->setValueOptions($roleList);
                
            }elseif($userRole->getId() == 5){
                $allServices = $this->entityManager->getRepository(Service::class)
                        ->findBy(['id'=>$user->getService()], ['serviceName'=>'ASC']);
                $serviceList = [];
                foreach ($allServices as $service) {
                    $serviceList[$service->getId()] = $service->getServiceName();
                }
                $form->get('service')->setValueOptions($serviceList);
                
                $form->setData(array( 
                    'roles' => 4
                ));
            }
            else{
                $allServices = $this->entityManager->getRepository(Service::class)
                        ->findBy(['subDirectionId'=>$user->getService()], ['serviceName'=>'ASC']);
                $serviceList = [];
                foreach ($allServices as $service) {
                    $serviceList[$service->getId()] = $service->getServiceName();
                }
                $form->get('service')->setValueOptions($serviceList);
                
                $form->setData(array( 
                    'roles' => 5
                ));
            }
        }
        
        
        //Clique sur retour
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('annuler')){
            return $this->redirect()->toRoute('users', 
                        ['action'=>'index']);
        }
        
        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                // Get filtered and validated data
                $data = $form->getData();
                // Add user.
                $user = $this->userManager->addUser($data);
                
                // Redirect to "view" page
                return $this->redirect()->toRoute('users', 
                        ['action'=>'index']);                
            }               
        } 
        
        return new ViewModel([
                'form' => $form
            ]);
    }
    
    /**
     * The "view" action displays a page allowing to view user's details.
     */
    public function viewAction() 
    {
        $user = $this->currentUser();
                
        return new ViewModel([
            'user' => $user
        ]);
    }
    
    /**
     * The "edit" action displays a page allowing to edit user.
     */
    public function editAction() 
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $user = $this->entityManager->getRepository(User::class)
                ->find($id);
        
        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $userConnected = $this->currentUser();
        $userRoles = $userConnected->getRoles();
        
        foreach ($userRoles as $userRole){
            if($userRole->getId() == 1){
                $this->adminCheck = true;
            }
            else{
                $this->adminCheck = false;
            }
        }
        if (!$this->adminCheck){
            if($user->getService()!=$userConnected->getService()){
                $this->getResponse()->setStatusCode(404);
                return;
            }
        }
        
        // Create user form
        $form = new UserForm('update', $this->entityManager, $user, $userConnected);
        
        // Get the list of all available roles (sorted by name).
        foreach ($userRoles as $userRole){
            if($userRole->getId() == 1){
                
                $allServices = $this->entityManager->getRepository(Service::class)
                        ->findBy([], ['serviceName'=>'ASC']);
                $serviceList = [];
                foreach ($allServices as $service) {
                    $serviceList[$service->getId()] = $service->getServiceName();
                }
                $form->get('service')->setValueOptions($serviceList);

                $allRoles = $this->entityManager->getRepository(Role::class)
                        ->findBy([], ['name'=>'ASC']);
                $roleList = [];
                foreach ($allRoles as $role) {
                    $roleList[$role->getId()] = $role->getName();
                }
                $form->get('roles')->setValueOptions($roleList);

            }elseif($userRole->getId() == 5){
                $allServices = $this->entityManager->getRepository(Service::class)
                        ->findBy(['id'=>$user->getService()], ['serviceName'=>'ASC']);
                $serviceList = [];
                foreach ($allServices as $service) {
                    $serviceList[$service->getId()] = $service->getServiceName();
                }
                $form->get('service')->setValueOptions($serviceList);

            }
            else{
                $allServices = $this->entityManager->getRepository(Service::class)
                        ->findBy(['subDirectionId'=>$user->getService()], ['serviceName'=>'ASC']);
                $serviceList = [];
                foreach ($allServices as $service) {
                    $serviceList[$service->getId()] = $service->getServiceName();
                }
                $form->get('service')->setValueOptions($serviceList);

            }
        }
        
        //Clique sur retour
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('annuler')){
            return $this->redirect()->toRoute('users', 
                        ['action'=>'index']);
        }
        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            // Validate form
            if($form->isValid()) {
                
                // Get filtered and validated data
                $data = $form->getData();
                // Update the user.
                $this->userManager->updateUser($user, $data);
                
                // Redirect to "view" page
                return $this->redirect()->toRoute('users', 
                        ['action'=>'index']);                
            }               
        } else {
            
            $userRoleIds = [];
            foreach ($user->getRoles() as $role) {
                $userRoleIds[] = $role->getId();
            }
            
            $form->setData(array(
                    'job'=>$user->getJob(),
                    'function'=>$user->getFunction(),
                    'service'=>$user->getService(),
                    'full_name'=>$user->getFullName(),
                    'email'=>$user->getEmail(),
                    'status'=>$user->getStatus(), 
                    'roles' => $userRoleIds
                ));
        }
        
        return new ViewModel(array(
            'user' => $user,
            'form' => $form
        ));
    }
    
    /**
     * This action displays a page allowing to change user's password.
     */
    public function changePasswordAction() 
    {
        $user = $this->currentUser();
        
        // Create "change password" form
        $form = new PasswordChangeForm('change');
        
        //Clique sur retour
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('annuler')){
            return $this->redirect()->toRoute('users', 
                        ['action'=>'view']);
        }
        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                // Get filtered and validated data
                $data = $form->getData();
                
                // Try to change password.
                if (!$this->userManager->changePassword($user, $data)) {
                    $this->flashMessenger()->addErrorMessage(
                            'Désolé, l\'ancien mot de passe est incorrecte.');
                } else {
                    $this->flashMessenger()->addSuccessMessage(
                            'Le mot de passe a été modifié.');
                }
                
                // Redirect to "view" page
                return $this->redirect()->toRoute('users', 
                        ['action'=>'view']);                
            }               
        } 
        
        return new ViewModel([
            'user' => $user,
            'form' => $form
        ]);
    }
    
    /**
     * This action displays the "Reset Password" page.
     */
    public function resetPasswordAction()
    {
        // Create form
        $form = new PasswordResetForm($this->getRequest()->getBaseUrl());
        
        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                // Look for the user with such email.
                $user = $this->entityManager->getRepository(User::class)
                        ->findOneByEmail($data['email']);
                if ($user!=null && $user->getStatus() == User::STATUS_ACTIVE) {             
                    // Generate a new password for user and send an E-mail 
                    // notification about that.
                    $this->userManager->generatePasswordResetToken($user, $this->getRequest()->getBaseUrl());
                    
                    // Redirect to "message" page
                    return $this->redirect()->toRoute('users', 
                            ['action'=>'message', 'id'=>'sent']);                 
                } else {
                    return $this->redirect()->toRoute('users', 
                            ['action'=>'message', 'id'=>'invalid-email']);                 
                }
            }               
        } 
        
        $view = new ViewModel([                    
            'form' => $form
        ]);
        $view->setTerminal(true);
        
        return $view;
    }
    
    /**
     * This action displays an informational message page. 
     * For example "Your password has been resetted" and so on.
     */
    public function messageAction() 
    {
        // Get message ID from route.
        $id = (string)$this->params()->fromRoute('id');
        
        // Validate input argument.
        if($id!='invalid-email' && $id!='sent' && $id!='set' && $id!='failed') {
            throw new \Exception('Invalid message ID specified');
        }
        
        return new ViewModel([
            'id' => $id
        ]);
    }
    
    /**
     * This action displays the "Reset Password" page. 
     */
    public function setPasswordAction()
    {
        $email = $this->params()->fromQuery('email', null);
        $token = $this->params()->fromQuery('token', null);
        
        // Validate token length
        if ($token!=null && (!is_string($token) || strlen($token)!=32)) {
            throw new \Exception('Invalid token type or length');
        }
        
        if($token===null || 
           !$this->userManager->validatePasswordResetToken($email, $token)) {
            return $this->redirect()->toRoute('users', 
                    ['action'=>'message', 'id'=>'failed']);
        }
                
        // Create form
        $form = new PasswordChangeForm('reset');
        
        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            
            // Validate form
            if($form->isValid()) {
                
                $data = $form->getData();
                                               
                // Set new password for the user.
                if ($this->userManager->setNewPasswordByToken($email, $token, $data['new_password'])) {
                    
                    // Redirect to "message" page
                    return $this->redirect()->toRoute('users', 
                            ['action'=>'message', 'id'=>'set']);                 
                } else {
                    // Redirect to "message" page
                    return $this->redirect()->toRoute('users', 
                            ['action'=>'message', 'id'=>'failed']);                 
                }
            }               
        } 
        
        return new ViewModel([                    
            'form' => $form
        ]);
    }
}


