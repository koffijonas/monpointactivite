<?php
namespace User\Form;

use Zend\Form\Form;
use Zend\InputFilter\ArrayInput;
use User\Validator\UserExistsValidator;

/**
 * This form is used to collect user's email, full name, password and status. The form 
 * can work in two scenarios - 'create' and 'update'. In 'create' scenario, user
 * enters password, in 'update' scenario he/she doesn't enter password.
 */
class UserForm extends Form
{
    /**
     * Scenario ('create' or 'update').
     * @var string 
     */
    private $scenario;
    
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    private $entityManager = null;
    
    /**
     * Current user.
     * @var User\Entity\User 
     */
    private $user = null;
    
    /**
     * Current user.
     * @var User\Entity\User 
     */
    private $userConnected = null;
    
    /**
     * Constructor.     
     */
    public function __construct($scenario = 'create', $entityManager = null, $user = null, $userConnected = null)
    {
        // Define form name
        parent::__construct('user-form');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');
        
        // Save parameters for internal use.
        $this->scenario = $scenario;
        $this->entityManager = $entityManager;
        $this->user = $user;
        $this->userConnected = $userConnected;
        
        $this->addElements();
        $this->addInputFilter();          
    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        // Add "email" field
        
        $this->add([            
            'type'  => 'text',
            'name' => 'email',
            'options' => [
                'label' => 'E-mail',
            ],
        ]);
        
        $this->add([            
            'type'  => 'text',
            'name' => 'full_name',            
            'options' => [
                'label' => 'Nom et Prénom(s)',
            ],
        ]);
                
        $this->add([            
            'type'  => 'select',
            'name' => 'service',            
            'options' => [
                'label' => 'Service',
            ],
        ]);
                
        
        // Add "full_name" field
        foreach ($this->userConnected->getRoles() as $userRole){
            if($userRole->getId() == 1){
                // Add "full_name" field
                $this->add([            
                    'type'  => 'select',
                    'name' => 'function',            
                    'options' => [
                        'label' => 'Fonction',
                        'value_options' => [
                            'Agent' => 'Agent',
                            'Chef de service' => 'Chef de service',
                            'Sous-Directeur' => 'Sous-Directeur'
                        ]
                    ],
                ]);
                // Add "roles" field
                $this->add([            
                    'type'  => 'select',
                    'name' => 'roles',
                    'attributes' => [
                        'multiple' => 'multiple',
                    ],
                    'options' => [
                        'label' => 'Role(s)',
                    ],
                ]);

            }
            elseif($userRole->getId() == 5){
                // Add "full_name" field
                $this->add([            
                    'type'  => 'select',
                    'name' => 'function',            
                    'options' => [
                        'label' => 'Fonction',
                        'value_options' => [
                            'Agent' => 'Agent',
                        ]
                    ],
                    
                ]);
                
                $this->add([            
                    'type'  => 'select',
                    'name' => 'roles',
                    'attributes' => [
                        'multiple' => 'multiple',
                    ],
                    'options' => [
                        'label' => 'Role(s)',
                        'value_options' => [
                            '4' => 'Agent',
                        ]
                    ],
                ]);
            }
            else{
                $this->add([            
                    'type'  => 'select',
                    'name' => 'function',            
                    'options' => [
                        'label' => 'Fonction',
                        'value_options' => [
                            'Chef de service' => 'Chef de service',
                        ]
                    ],
                    
                ]);
                
                $this->add([            
                    'type'  => 'select',
                    'name' => 'roles',
                    'attributes' => [
                        'multiple' => 'multiple',
                    ],
                    'options' => [
                        'label' => 'Role(s)',
                        'value_options' => [
                            '5' => 'Chef de service',
                        ]
                    ],
                ]);
            
            }
            
        }
        
        // Add "full_name" field
        $this->add([            
            'type'  => 'select',
            'name' => 'job',            
            'options' => [
                'label' => 'Emploi',
                'value_options' => [
                    'Technicien Supérieur de l\'informatique' => 'Technicien Supérieur de l\'Informatique',
                    'Ingenieur Informaticien' => 'Ingenieur Informaticien',
                    'Ingenieur Principal en Informatique' => 'Ingenieur Principal en Informatique',
                ]
            ],
        ]);
        
        
        if ($this->scenario == 'create') {
        
            // Add "password" field
            $this->add([            
                'type'  => 'password',
                'name' => 'password',
                'options' => [
                    'label' => 'Mot de passe',
                ],
            ]);
            
            // Add "confirm_password" field
            $this->add([            
                'type'  => 'password',
                'name' => 'confirm_password',
                'options' => [
                    'label' => 'Confirmez le mot de passe',
                ],
            ]);
        }
        
        // Add "status" field
        $this->add([            
            'type'  => 'select',
            'name' => 'status',
            'options' => [
                'label' => 'Statut',
                'value_options' => [
                    1 => 'Activer',
                    2 => 'Désactiver',                    
                ]
            ],
        ]);
        
        
        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600
                ]
            ],
        ]);
        
        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'annuler',
            'attributes' => [                
                'value' => 'Retour'
            ],
        ]);
        
        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [                
                'value' => 'Créer'
            ],
        ]);
    }
    
    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter() 
    {
        // Create main input filter
        $inputFilter = $this->getInputFilter();        
                
        // Add input for "email" field
        $inputFilter->add([
                'name'     => 'email',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],                    
                ],                
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 1,
                            'max' => 128
                        ],
                    ],
                    [
                        'name' => 'EmailAddress',
                        'options' => [
                            'allow' => \Zend\Validator\Hostname::ALLOW_DNS,
                            'useMxCheck'    => false,                            
                        ],
                    ],
                    [
                        'name' => UserExistsValidator::class,
                        'options' => [
                            'entityManager' => $this->entityManager,
                            'user' => $this->user
                        ],
                    ],                    
                ],
            ]);     
        
        // Add input for "full_name" field
        $inputFilter->add([
                'name'     => 'full_name',
                'required' => false,
                'filters'  => [                    
                    ['name' => 'StringTrim'],
                ],                
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 1,
                            'max' => 512
                        ],
                    ],
                ],
            ]);
        
        if ($this->scenario == 'create') {
            
            // Add input for "password" field
            $inputFilter->add([
                    'name'     => 'password',
                    'required' => true,
                    'filters'  => [                        
                    ],                
                    'validators' => [
                        [
                            'name'    => 'StringLength',
                            'options' => [
                                'min' => 6,
                                'max' => 64
                            ],
                        ],
                    ],
                ]);
            
            // Add input for "confirm_password" field
            $inputFilter->add([
                    'name'     => 'confirm_password',
                    'required' => true,
                    'filters'  => [                        
                    ],                
                    'validators' => [
                        [
                            'name'    => 'Identical',
                            'options' => [
                                'token' => 'password',                            
                            ],
                        ],
                    ],
                ]);
        }
        
        // Add input for "status" field
        $inputFilter->add([
                'name'     => 'status',
                'required' => true,
                'filters'  => [                    
                    ['name' => 'ToInt'],
                ],                
                'validators' => [
                    ['name'=>'InArray', 'options'=>['haystack'=>[1, 2]]]
                ],
            ]); 
        
        // Add input for "roles" field
        $inputFilter->add([
                'class'    => ArrayInput::class,
                'name'     => 'roles',
                'required' => true,
                'filters'  => [                    
                    ['name' => 'ToInt'],
                ],                
                'validators' => [
                    ['name'=>'GreaterThan', 'options'=>['min'=>0]]
                ],
            ]); 
    }           
}