function refresh(sourceUrl, dtId, newBtnName,bFilter) {
 
    $languageUrl = getLanguageUrl();
    $params = {
        sDom: "<'row '<'col-xs-6'l><'col-xs-6'f>r>" + "t" + "<'row'<'col-xs-12'pi>>",
        bLengthChange: false,
        iDisplayLength: 10,
        bFilter: bFilter || false,
        bInfo: true,
        bProcessing: true,
        bServerSide: true,
        sAjaxSource: sourceUrl,
        sServerMethod: 'post',
        oLanguage: {
            sUrl: $languageUrl,
        },
        fnRowCallback: fonctionRowCallBack
    };
    
    if (typeof oTable == 'undefined') {
        oTable = $('#'+dtId).dataTable($params);
    } else {
        oTable.fnClearTable(0);
        oTable.fnDraw();
    }
    
    $('button[name='+newBtnName+']').show();
}

function create() {
 
    var oForm = $($formulaire);
    oForm.load($urlCrudForm,function(){
        $('button[name='+$btnName+']').hide();
    });  
    oForm.show();
}

/**
 * @returns void
 */
function formCancel() {
    var oForm = $($formulaire);
    oForm.empty();
    oForm.removeAttr('param');
    
    if ($('button[name='+$btnName+']').length == 1) {
        $('button[name='+$btnName+']').show();
    } else { 
        oForm.append(
            $('<div class="row">\n\
                    <div class="col-lg-12 text-center">\n\
                        <button type="button" class="btn btn-primary" onclick="create();" name="'+$btnName+'" id="bouton-nouvel-element">Nouveau <span class="glyphicon glyphicon-plus"></span></button>\n\
                    </div>\n\
               </div>'));
    }
}

function formDelete() {
    
    var oForm = $($formulaire);
    var postUrl;
     
    
    if(oForm.attr('param')!== undefined && oForm.attr('param')!== null) {
        postUrl =   $deleteUrl + '/id/' +oForm.attr('param');
    }else{
        alert('Vous devez sélectionner un élément à supprimer');
        return false; 
    }
    
    $.post(postUrl ,'').done(function(content) {
        console.log(content)
        if (content == '1') {
            alert('élément supprimé');
            oForm.hide();
        } else {
            oForm.find('.panel').attr('class','panel panel-danger');
            oForm.find('.panel .panel-title').html('Echec de suppression');
        }
        refresh($('#dataUrl').attr('value'),$dt,$btnName); 
    });
    
}

/**
 * 
 * @param string formName
 * @returns void
 */
function formValidate(formName) {
    
    var oForm = $($formulaire);
    var postUrl;
    
    if(oForm.attr('param')!== undefined && oForm.attr('param')!== null) {
        postUrl =   $urlCrudForm+ '/id/' +oForm.attr('param');
    }else{
        postUrl =   $urlCrudForm; 
    }
    
    // cette méthode fonctionne avec les pieces jointes.
    $f = document.getElementById(formName);
    $.ajax({
        url: postUrl,
        type: 'post',
        cache: false,
        contentType: false,
        processData: false,
        data:  new FormData($f) 
   }).done(function(content) {
        oForm.empty().append(content);
        if (content.indexOf('panel-danger') == -1) {
            $($formulaire).hide();
            alert('Information enregistrée.');
        }
        refresh($('#dataUrl').attr('value'),$dt,$btnName);
        
    }).fail(function(content) {
        oForm.empty().append(content);
        refresh($('#dataUrl').attr('value'),$dt,$btnName); // aussi en cas de faux bug qui donne le resultat aussi
    });
}

function chargement_ligne() {
    
    if (row = oTable.fnGetData(this)) {
        var id = oTable.fnGetData(this)[0];
        var oForm = $($formulaire);
        
        oForm.load($urlCrudForm+ '/id/' + id,function() {
            // des choses Si necessaire
        });
        $('button[name='+$btnName+']').hide();
        oForm.attr('param', id);
        oForm.show(); 
    }
}