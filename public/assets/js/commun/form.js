function passageValeurMultiple($elements,$name) {

    $str = '';

    if ($elements.length ==1 ) {
       if ($elements[0] != '') {
           $str += '/'+$name+'/'+$elements[0];
       }
    } else {
       if ($elements.length > 1) {
           $i = 0
           while ( $i < $elements.length) {

               $str += '/'+$name+'/'+$elements[$i];
               $i++;
           }
       }
   }
   
   return $str;
}


$(document).ready(function(){
    
    /**
    * Fonction qui permet d'avoir un switch select/multiselect dans les formulaires en cliquant sur une image. 
    * Marche avec le Projet_Form_Element_Switchselect
    */
    $('.plus-btn').click(function(){
    
        var menu = $(this).closest('.row').find('select');
        if (menu.attr('multiple')) {
            menu.removeAttr('multiple');
            menu.attr('name',menu.attr('name').replace('[]',''));
          
        } else {
            menu.attr('multiple', 'multiple');
            menu.attr('name',menu.attr('name')+'[]');
          
        }
        
    });
         
    
}); 