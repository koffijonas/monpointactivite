function switchTypePersonne() {

    if ( $('#typePersonne').is(':checked')) {
        
        $("#nccTiers")           .parents('.form-group').show();
        $("#idTypeTiers")        .parents('.form-group').show();
        $("#raisonSocialeTiers") .parents('.form-group').show();

        $("#nom")                .parents('.form-group').hide();
        $("#prenom")             .parents('.form-group').hide();
        $("#typePiece")          .parents('.form-group').hide();
        $("#numeroPiece")        .parents('.form-group').hide();
        
    } else {
        
        $("#nccTiers")           .parents('.form-group').hide();
        $("#idTypeTiers")        .parents('.form-group').hide();
        $("#raisonSocialeTiers") .parents('.form-group').hide();

        $("#nom")                .parents('.form-group').show();
        $("#prenom")             .parents('.form-group').show();
        $("#typePiece")          .parents('.form-group').show();
        $("#numeroPiece")        .parents('.form-group').show();
    }

}
