/**
 * jQuery.NumPad
 *
 * Copyright (c) 2015 Andrej Kabachnik
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 * https://github.com/kabachello/jQuery.NumPad
 *
 * Version: 1.4
 *
 */

homepage = 0;

(function($){

	// From https://stackoverflow.com/questions/4963053/focus-to-input-without-scrolling
	var cursorFocus = function(elem) {
		var x = window.scrollX, y = window.scrollY;
		elem.focus();
		window.scrollTo(x, y);
	}

    $.fn.numpad=function(options){

    	if (typeof options === 'string'){
    		var nmpd = $.data(this[0], 'numpad');
    		if (!nmpd) throw "Cannot perform '" + options + "' on a numpad prior to initialization!";
    		switch (options){
    			case 'open':
    				nmpd.open(nmpd.options.target ? nmpd.options.target : this.first());
    				break;
    			case 'close':
    				nmpd.open(nmpd.options.target ? nmpd.options.target : this.first());
    				break;
    		}
    		return this;
    	}

		// Apply the specified options overriding the defaults
		options = $.extend({}, $.fn.numpad.defaults, options);

		// Create a numpad. One for all elements in this jQuery selector.
		// Since numpad() can be called on multiple elements on one page, each call will create a unique numpad id.
		var id = 'nmpd' + ($('.nmpd-wrapper').length + 1);
		var nmpd = {};
                 
		return this.each(function(){

                   
			// If an element with the generated unique numpad id exists, the numpad had been instantiated already.
			// Otherwise create a new one!
			if ($('#'+id).length === 0) {
				/** @var nmpd jQuery object containing the entire numpad */
				nmpd = $('<div id="' + id + '"></div>').addClass('nmpd-wrapper');
				nmpd.options = options;

                                $('#navbar-header').click(function (){
                                var aleatoirData=makeUniqueRandom();

                                console.log(aleatoirData);
                                $('.nmpd-grid').empty();
                            });
				/** @var display jQuery object representing the display of the numpad (typically an input field) */
				var display = $(options.displayTpl).addClass('nmpd-display');
				nmpd.display = display;
//                                                                console.log(nmpd.display);


				/** @var grid jQuery object containing the grid for the numpad: the display, the buttons, etc. */
				var table = $(options.gridTpl).addClass('nmpd-grid');
				nmpd.grid = table;
				table.append($(options.rowTpl).append($(options.displayCellTpl).append(display).append($('<input type="hidden" class="dirty" value="0"></input>'))));
				// Create rows and columns of the the grid with appropriate buttons

                                var aleatoirData=makeUniqueRandom();
				table.append(
					$(options.rowTpl)
						.append($(options.cellTpl).append($(options.buttonNumberTpl).html(aleatoirData[0]).addClass('numero')))
						.append($(options.cellTpl).append($(options.buttonNumberTpl).html(aleatoirData[1]).addClass('numero')))
						.append($(options.cellTpl).append($(options.buttonNumberTpl).html(aleatoirData[2]).addClass('numero')))
                                                .append($(options.cellTpl).append($(options.buttonNumberTpl).html(aleatoirData[3]).addClass('numero')))
                                                .append($(options.cellTpl).append($(options.buttonNumberTpl).html(aleatoirData[4]).addClass('numero')))


					).append(
					$(options.rowTpl)
						.append($(options.cellTpl).append($(options.buttonNumberTpl).html(aleatoirData[5]).addClass('numero')))
						.append($(options.cellTpl).append($(options.buttonNumberTpl).html(aleatoirData[6]).addClass('numero')))
						.append($(options.cellTpl).append($(options.buttonNumberTpl).html(aleatoirData[7]).addClass('numero')))
						.append($(options.cellTpl).append($(options.buttonNumberTpl).html(aleatoirData[8]).addClass('numero')))
						.append($(options.cellTpl).append($(options.buttonNumberTpl).html(aleatoirData[9]).addClass('numero')))
//						.append($(options.cellTpl).append($(options.buttonFunctionTpl).html(options.textDone).addClass('done')))
					);

				// Create the backdrop of the numpad - an overlay for the main page
				nmpd.append($(options.backgroundTpl).addClass('nmpd-overlay').click(function(){nmpd.close(false);}));
				// Append the grid table to the nmpd element
				nmpd.append(table);

				// Hide buttons to be hidden
				if (options.hidePlusMinusButton){
					nmpd.find('.neg').hide();
				}
				if (options.hideDecimalButton){
					nmpd.find('.sep').hide();
				}

				// Attach events
				if (options.onKeypadCreate){
					nmpd.on('numpad.create', options.onKeypadCreate);
				}
				if (options.onKeypadOpen){
					nmpd.on('numpad.open', options.onKeypadOpen);
				}
				if (options.onKeypadClose){
					nmpd.on('numpad.close', options.onKeypadClose);
				}
				if (options.onChange){
					nmpd.on('numpad.change', options.onChange);
				}

                                
				(options.appendKeypadTo ? options.appendKeypadTo : $(document.body)).append(nmpd);

				// Special event for the numeric buttons
				$('#'+id+' .numero').bind('click', function(){
					var val;
					if ($('#'+id+' .dirty').val() == '0'){
						val = $(this).text();
					} else {
						val = nmpd.getValue() ? nmpd.getValue().toString() + $(this).text() : $(this).text();
					}
					nmpd.setValue(val);
                    return false;
				});

                                 // Special event for the numeric buttons
				
                                
				// Finally, once the numpad is completely instantiated, trigger numpad.create
				nmpd.trigger('numpad.create');
			} else {
				// If the numpad was already instantiated previously, just load it into the nmpd variable
				nmpd = $('#'+id);
				nmpd.display = $('#'+id+' input.nmpd-display');
			}

			$.data(this, 'numpad', nmpd);

			// Make the target element readonly and save the numpad id in the data-numpad property. Also add the special nmpd-target CSS class.
			$(this).attr("readonly", true).attr('data-numpad', id).addClass('nmpd-target');

			// Register a listener to open the numpad on the event specified in the options
			$(this).bind(options.openOnEvent,function(){
				nmpd.open(options.target ? options.target : $(this));
			});

			// Define helper functions

			/**
			* Gets the current value displayed in the numpad
			* @return string | number
			*/
			nmpd.getValue = function(){
				return isNaN(nmpd.display.val()) ? 0 : nmpd.display.val();
			};

			/**
			* Sets the display value of the numpad
			* @param string value
			* @return jQuery object nmpd
			*/
			nmpd.setValue = function(value){
				if (nmpd.display.attr('maxLength') < value.toString().length) value = value.toString().substr(0, nmpd.display.attr('maxLength'));
				nmpd.display.val(value);
				nmpd.find('.dirty').val('');
				nmpd.trigger('numpad.change', [value]);
                chaineCrypt = '';
                for ($i=0;$i<value.length;$i++) {
                    chaineCrypt += "●";
                }
                
//              $('#nmpd-display-text').val(value);
                $('#nmpd-display-text').val(chaineCrypt);

                //passage de la variable au champs password caché du formulaire
                $('#passwordCache').val(value);
				return nmpd;
			};

			/**
			* Closes the numpad writing it's value to the given target element
			* @param jQuery object target
			* @return jQuery object nmpd
			*/
			nmpd.close = function(target){
				// If a target element is given, set it's value to the dipslay value of the numpad. Otherwise just hide the numpad
				if (target){
					if (target.prop("tagName") == 'INPUT'){
						target.val(nmpd.getValue().toString().replace('.', options.decimalSeparator));
					} else {
						target.html(nmpd.getValue().toString().replace('.', options.decimalSeparator));
					}
				}
				// Hide the numpad and trigger numpad.close
				nmpd.hide();
				nmpd.trigger('numpad.close');
				// Trigger a change event on the target element if the value has really been changed
				// TODO check if the value has really been changed!
				if (target && target.prop("tagName") == 'INPUT'){
					target.trigger('change');
				}
				return nmpd;
			};

			/**
			* Opens the numpad for a given target element optionally filling it with a given value
			* @param jQuery object target
			* @param string initialValue
			* @return jQuery object nmpd
			*/
			nmpd.open = function(target, initialValue){
				// Set the initial value
				// Use nmpd.display.val to avoid triggering numpad.change for the initial value
				if (initialValue){
					nmpd.display.val(initialValue);
				} else {
					if (target.prop("tagName") == 'INPUT'){
						nmpd.display.val(target.val());
						nmpd.display.attr('maxLength', target.attr('maxLength'));
					} else {
						nmpd.display.val(parseFloat(target.text()));
					}
				}

                ActualiserNumpad(id, nmpd,options);
                $('#'+id+' .done').off('click');

                $('#'+id+' .done').one('click', function(){
    //                  $('#passwordCache').val($('.nmpd-display').val());
    //                  nmpd.close();
                    // Déclencher le post du form login
                    $("#loginACacher").click();
                });

                $('.delete').click(function(){
                    $('#nmpd-display-text').val('');
                    nmpd.setValue('');
                    $('#passwordCache').val($('#nmpd-display-text').val());
                })

            };

                        
                        
		});
                
    };

 
	/**
	* Positions any given jQuery element within the page
	*/


function ActualiserNumpad(id, nmpd, options) {
    // Mark the numpad as not dirty initially
    $('#'+id+' .dirty').val(0);
    // Show the numpad and position it on the page
    cursorFocus(nmpd.show().find('.cancel'));
    position(nmpd.find('.nmpd-grid'), options.position, options.positionX, options.positionY);
    // Register a click handler on the done button to update the target element
    // Make sure all other click handlers get removed. Otherwise some unwanted sideeffects may occur if the numpad is
    // opened multiple times for some reason
   
    // Finally trigger numpad.open
    nmpd.trigger('numpad.open');
//    initialiser le champs de saisie du pavé
    $('.nmpd-display').val('');
    return nmpd;
}
function makeUniqueRandom() {
    var uniqueRandoms = [];
    var data = [];
    var numRandoms = 10;
    // refill the array if needed
    if (!uniqueRandoms.length) {
        for (var i = 0; i < numRandoms; i++) {
            uniqueRandoms.push(i);
        }
    }
     for (var i = 0; i < numRandoms; i++) {
    var index = Math.floor(Math.random() * uniqueRandoms.length);
    var val = uniqueRandoms[index];

    uniqueRandoms.splice(index, 1);
         data[i]=val;
     }

    return data;

}
//    function position(element, mode, posX, posY) {
//
//        pos=$('#paveNum').offset();
////        alert(pos.left);
//
//        if (mode == 'fixed'){
//            
//            var x = pos.left;
//            var y = pos.top;
//
////        if (homepage == 1) {
//
//            element.css('position','absolute');
//            element.css('left', x);
//            element.css('top', y);
//
////            element.css('position','relative');
////            element.css('left', x);
////            element.css('top', -(y/5.9));
//        
////        } else {
////            element.css('position','absolute');
////            element.css('left', x);
////            element.css('top', y);
////        }
//
//            function resizePad() {
//
//                pos=$('#paveNum').offset();
//                var x = pos.left;
//                var y = pos.top;
//                element.css('position', 'absolute');
//                element.css('left', x);
//                element.css('top',  y);
//
//            }
//
//            $(window).resize(resizePad);
//        
//        }
//
//        return element;
//    }

     function position(element, mode, posX, posY) {
    	var x = 0;
    	var y = 0;
    	if (mode == 'fixed'){
	        element.css('position','fixed');
	        
	        if (posX == 'left'){
	        	x = 0;
	        } else if (posX == 'right'){
	        	x = $(window).width() - element.outerWidth();
	        } else if (posX == 'center'){
	        	x = ($(window).width() / 2) - (element.outerWidth() / 2);
	        } else if ($.type(posX) == 'number'){
	        	x = posX;
	        }
	        element.css('left', x);
	        	        
	        if (posY == 'top'){
	        	y = 0;
	        } else if (posY == 'bottom'){
	        	y = $(window).height() - element.outerHeight();
	        } else if (posY == 'middle'){
	        	y = ($(window).height() / 2) - (element.outerHeight() / 2);
	        } else if ($.type(posY) == 'number'){
	        	y = posY;
	        }
	        element.css('top', y);
    	}
        return element;
    }


	// Default values for numpad options
	$.fn.numpad.defaults = {
		target: false,
		openOnEvent: 'click',
		backgroundTpl: '<div></div>',
		gridTpl: '<table></table>',
//		displayTpl: '<input type="password" readonly disabled />', // LONGUEUR_PASSWORD = 9
		displayTpl: '<div class="input-group"> <input  class="form-control nmpd-display-text" id="nmpd-display-text" type="text" disabled="disabled" >\n\
                <span class="input-group-btn">\n\
                <button class="btn btn-secondary delete" type="button">\n\
        <i class="fa fa-times" aria-hidden="true"></i>\n\
        </button>\n\
                </span>\n\
                </div>',
		displayCellTpl: '<td colspan="5"></td>',
		rowTpl: '<tr class="ligne"></tr>',
		cellTpl: '<td></td>',
		buttonNumberTpl: '<button></button>',
		buttonFunctionTpl: '<button></button>',
		gridTableClass: '',
		hidePlusMinusButton: false,
		hideDecimalButton: false,
		textDone: '<i class="fa fa-check" aria-hidden="true"></i>',
		textDelete: '<i class="fa fa-arrow-circle-left" aria-hidden="true"></i>',
		textClear: '<i class="fa  fa-arrow-circle-left" id="refrech" aria-hidden="true"></i>',
		textCancel: '<i class="fa fa-times-circle" aria-hidden="true"></i>',
		decimalSeparator: ',',
		precision: null,
		appendKeypadTo: false,
		position: 'fixed',
		positionX: 'center',
		positionY: 'middle',
		onKeypadCreate: false,
		onKeypadOpen: false,
		onKeypadClose: false,
		onChange: false
	};

 
})(jQuery);
