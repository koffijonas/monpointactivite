/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    
$(document).ready(function() {
    $('#tableau').DataTable({
        //
        sDom : "<'top row'<'col-sm-2'l><'col-sm-7 ctnCmd'><'col-sm-3'f> >rt<'bottom'ip<'clear'>>",
        bLengthChange: true,
        iDisplayLength: 10,
        sAjaxSource: basePath + '/ajax/mes-activites',
        sServerMethod: 'post',
        fnServerData: function ( sSource, aoData, fnCallback, oSettings ) {
            oSettings.jqXHR = $.ajax( {
              "dataType": 'json',
              "type": "POST",
              "url": sSource,
              "data": {start:$('#start').val(), end:$('#end').val(), executionState:$('#executionState').val()},
              "success": fnCallback
        } )},
        aaSorting: [[7, 'desc']],
        oLanguage: {sUrl: basePath + '/assets/js/jquery.dataTables/jquery.dataTables.fr.json'},
        fnRowCallback: function(nRow, aData, iDisplayIndex){
            //alert(aData['id']);
            $('td:eq(0)', nRow).html(aData[0]);
            $('td:eq(1)', nRow).html('<span id="activite'+aData[6]+'">'+aData[1]+'</span>');
            $('td:eq(2)', nRow).html(aData[7]=='0000-00-00'?'-': formatDateMysql(aData[7]) );
            $('td:eq(3)', nRow).html(formatDateMysql(aData[2]));
            $('td:eq(4)', nRow).html(aData[3]=='0000-00-00'?'-': formatDateMysql(aData[3]) );
            $('td:eq(5)', nRow).html(getStatutFag(aData[4]));
            $('td:eq(6)', nRow).html(alignCenter(aData[8]+'%'));
            $('td:eq(7)', nRow).html(aData[5]);
            $('td:eq(8)', nRow).html(getBtn(aData));
            
        },
        drawCallback: function( aData ) {
            $('.ctnCmd').html(
                    '<a href="'+basePath+'/pointactivites/add" class="btn btn-primary btn-xs" style="border:1px; margin:3px 10px;">'+
                    '    <button type="button" class="btn-primary btn-xs" style="background: #FFF; border: 1px; color: #428bca; margin-left: -4px"><i class="fa fa-plus"></i></button>'+
                    '    &nbsp;&nbsp;&nbsp;<b>Ajouter une activité</b>&nbsp;&nbsp;&nbsp;'+
                    '</a>'+
                    '<a id="exportExcel2" href="#" class="btn btn-success btn-xs" style="border:1px; margin:3px 10px;">'+
                    '    <button type="button" class="btn-primary btn-xs" style="background: #FFF; border: 1px; color: green; margin-left: -4px"><i class="fa fa-download"></i></button>'+
                    '    &nbsp;&nbsp;&nbsp;<b>Excel</b>&nbsp;&nbsp;&nbsp;'+
                    '</a>'
            );
            $('#exportExcel2').on('click',function(){
                $("#frmExportExcel").submit();
                return false;
            });
        }
    });
    
    /*$("#exportExcel").attr('href', basePath + "/ajax/exportExcel");
    
    start = escape($('#start').val());
    end = escape($('#end').val());
    if(start != ""){
        $("#exportExcel").attr('href', basePath + "/ajax/exportExcel/" + start);
        if(end != ""){
            $("#exportExcel").attr('href', basePath + "/ajax/exportExcel/" + start + "/" + end);
        }
    }*/
    $('#exportExcel').on('click',function(){
        $("#frmExportExcel").submit();
        return false;
    });
    
    
});

function escape(data)
{
    return data.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "");
}

function getStatutFag(statut){
    if(statut=='Terminé'){
        return '<span class="label label-success" style="font-size:inherit;font-weight:inherit">'+statut+'</span>';
        
    }else if(statut=='En cours'){
        return '<span class="label label-warning" style="font-size:inherit;font-weight:inherit">'+statut+'</span>';
        
    }else{
        return '<span class="label label-default" style="font-size:inherit;font-weight:inherit">'+statut+'</span>';
    }
}
    
function getBtn(aData){
    
    var btnDelete = "<td style='text-align:center;' class='menu-action rounded-btn'>" +
            "    <nobr>" +
            "        <a data-hint='Modifier' href='"+basePath+"/pointactivites/edit/"+aData[6]+"'" +
            "            data-placement='top' class='btn btn-xs menu-icon vd_bd-green vd_green hint--left'> " +
            "             <i class='fa fa-pencil-square-o fa-2x'></i> " +
            "        </a> " +
            "        <a data-hint='collaborateurs sur cette activité' id='btn"+aData[6]+"' data-placement='top'" +
            "            href='javascript:collabModal("+aData[6]+")' class='btn btn-xs menu-icon vd_bd-red vd_red hint--top'> " +
            "             <i class='fa fa-users fa-2x'></i> " + (aData[8]>0? "<span class='label label-danger myBadge'>"+aData[8]+"</span> " : '') +
            "        </a>";
    
    if(aData[9] == 0){
        btnDelete += "        <a data-hint='Supprimer' data-toggle='tooltip' data-placement='top'" +
            "            href='javascript:deleteAction("+aData[6]+")' class='btn btn-xs menu-icon vd_bd-red vd_red hint--top'> " +
            "             <i class='fa fa-trash fa-2x'></i> " +
            "        </a>" +
            "    </nobr>  " +                
            "</td>";
    }
    
    return btnDelete;
}

function collabModal(activite){
    $('#modal-collaborateurs').modal();
    $('#bodyModalCollab').html( $('#loadingAnimation').html() );
    
    $.ajax({
        method: "POST",
        url: basePath + '/ajax/collaborateurs-sur-point',
        data: 'id=' + activite
    })
    .done(function( collaborateurs ) {
        //alert(collaborateurs);
        var html = '<div class="row"> <div class="col-sm-12"> <b>Activité</b> :  '+$('#activite'+activite).html()+' </div> </div><br/>';
        html += '<input type="hidden" name="activiteId" value="'+activite+'" />';
        var listeCollab = collaborateurs.listeCollab;
        for(var i=0;i<listeCollab.length;i++){
            checked = listeCollab[i].collabId == null ? '' : 'checked="checked"';
            if(collaborateurs.propietaireId==listeCollab[i].userId){
                html += '<div class="row"> \n\
                            <div class="col-sm-2"></div> \n\
                            <div class="col-sm-8" style="border:1px #EEE solid; margin-top:5px"> \n\
                                <h4> <input checked="checked" disabled="disabled" type="checkbox" name="collab'+i+'" id="collab'+i+'" value="'+listeCollab[i].userId+'" />&nbsp;&nbsp; <i class="fa fa-user-secret"></i> <label for="collab'+i+'" style="font-size:16px">'+listeCollab[i].fullName+'</label></h4> \n\
                            </div> \n\
                            <div class="col-sm-2"> </div> \n\
                        </div>';
            }else{
                html += '<div class="row"> \n\
                            <div class="col-sm-2"></div> <input type="hidden" name="_collab'+i+'" value="'+listeCollab[i].collabId+'" /> \n\
                            <div class="col-sm-8" style="border:1px #EEE solid; margin-top:5px"> \n\
                                <h4> <input '+checked+' type="checkbox" name="collab'+i+'" id="collab'+i+'" value="'+listeCollab[i].userId+'" />&nbsp;&nbsp; <i class="fa fa-user"></i> <label for="collab'+i+'" style="font-size:16px">'+listeCollab[i].fullName+'</label></h4> \n\
                            </div> \n\
                            <div class="col-sm-2"> </div> \n\
                        </div>';
            }
        }
        
        setTimeout(function(){
            $('#bodyModalCollab').html(html);
        }, 500)
        
    })
    /*.error(function(jqXHR, textStatus, errorThrown ){
              alert(textStatus+' : '+errorThrown);  
    })*/
    ;
    
}

function close(modalId){
    $('#'+modalId).modal('hide');
}

function deleteAction(id){
    $('#confirmer-suppression').modal('show');
    $("#confirmer-btn-mandat").attr('href', basePath + '/pointactivites/delete/'+id);
}

function exportToExcel(){
    var today = new Date();
    var twoDigitMonth = ((today.getMonth().length+1) === 1)? (today.getMonth()+1) : '0' + (today.getMonth()+1);
    var filename = today.getFullYear()+"_"+twoDigitMonth+"_"+today.getDate();

    $('#tableau').table2excel({
        name: "Point d'activité",
        filename: filename,
        fileext: ".xls",
        exclude_img: true,
        exclude_links: true,
        exclude: ".dntinclude",
        exclude_inputs: true
    });
 
}

function exportWord() {
 var today = new Date();
    var twoDigitMonth = ((today.getMonth().length+1) === 1)? (today.getMonth()+1) : '0' + (today.getMonth()+1);
    var filename = today.getFullYear()+"_"+twoDigitMonth+"_"+today.getDate();
   if (!window.Blob) {
      alert('Your legacy browser does not support this action.');
      return;
   }

   var html, link, blob, url, css;
   
   // EU A4 use: size: 841.95pt 595.35pt;
   // US Letter use: size:11.0in 8.5in;
   
   css = (
     '<style>' +
     '@page WordSection1{size: 841.95pt 595.35pt;mso-page-orientation: landscape;}' +
     'div.WordSection1 {page: WordSection1;}' +
     'table{border-collapse:collapse;}td{border:1px gray solid;width:5em;padding:2px;}'+
     '</style>'
   );
   
   html = window.docx.innerHTML;
   blob = new Blob(['\ufeff', css + html], {
     type: 'application/msword'
   });
   url = URL.createObjectURL(blob);
   link = document.createElement('A');
   link.href = url;
   // Set default file name. 
   // Word will append file extension - do not add an extension here.
   link.download = 'Document';   
   document.body.appendChild(link);
   if (navigator.msSaveOrOpenBlob ) navigator.msSaveOrOpenBlob( blob, filename); // IE10-11
   		else link.click();  // other browsers
   document.body.removeChild(link);
 }

function formatDateMysql(elem) {
    if (!elem) return '-';
    //$part = $elem.split(' ');
    $part_date = elem.split('-');
    return $part_date[2]+'/'+$part_date[1]+'/'+$part_date[0];
}
