/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    $('#exportExcel, #exportExcel2').on('click',function(){
        $('#frmExportExcel').submit();
        return false;
    });
    
    $('#validatePoints, #validatePoints2').on('click',function(){
        if( confirm("Vous êtes sur le point de valider les points d'activités de votre service. \n\n Voulez-vous vraiment les validés ?") ){
            $('#frmValidatePoints').submit();
        }
        
        return false;
    });
    
});
